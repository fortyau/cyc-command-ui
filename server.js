// Ensure environment variables are read.
require('./config/env');
const express = require('express')
const fs = require('fs')
const https = require('https')
const forceSSL = require('express-force-ssl');
const paths = require('./config/paths')
const PORT = process.env.PORT || 11094
const SSL_KEY_PATH = process.env.SSL_KEY_PATH
const SSL_CERT_PATH = process.env.SSL_CERT_PATH

const app = express()

app.use(express.static(paths.appBuild))
app.set('view engine', 'html')

app.get('*', (req, res) => {
    res.sendFile(`${paths.appBuild}/index.html`, {})
})

if (process.env.NODE_ENV === 'development') {
    app.listen(PORT)
} else {
    if ( fs.existsSync(SSL_KEY_PATH) && fs.existsSync(SSL_CERT_PATH))
    {
        app.use(forceSSL)
        const key = fs.readFileSync(SSL_KEY_PATH)
        const cert = fs.readFileSync(SSL_CERT_PATH)
        const options = { key, cert }

        https.createServer(options, app).listen(PORT, () => {
            console.log('Server running on port', PORT)
        })
    }
    else {
        console.log('Error with ssl. Signing authority not found.')
    }
}
