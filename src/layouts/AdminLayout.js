import React from 'react'

class AdminLayout extends React.Component {
    render () {
        const {children} = this.props
        return (
            <div>
                {children}
            </div>
        )
    }
}

export default AdminLayout
