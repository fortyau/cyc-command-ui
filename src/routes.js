import React from 'react'
import Reflux from 'reflux'
import { Route, Switch, Redirect } from 'react-router-dom'
import ClaimDetails from "./views/Claims/Details";
import ExternalTransferDetails from "./views/ExternalTransfers";
import ProviderPaymentCards from './views/ProviderPaymentCards'
import Index from "./views/Index";
import Claims from "./views/Claims";
import AccountDetails from "./views/Accounts";
import Members from "./views/Members";
import EmployerDetail from "./views/Employers";
import QuickLookup from "./views/QuickLookup";
import QuickLookupContributions from "./views/QuickLookup/Contributions";
import QuickLookupEmployers from "./views/QuickLookup/Employers";
import FileMonitor from "./views/FileMonitor";
import FileMonitorDetails from "./views/FileMonitor/Details";
import Login from "./views/Login";
import Jobs from "./views/Jobs";
import Providers from "./views/Providers"

import AdminLayout from 'layouts/AdminLayout'
import Unauthorized from './views/Unauthorized'
import Security from 'data/security'

import SessionStore from 'stores/SessionStore'

class PrivateRoute extends Reflux.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.stores = [SessionStore]
    }

    render() {
        const {component: Component, ...rest} = this.props;
        return (
            <Route
                {...rest}
                render={props => {
                    const sec = new Security();
                    return sec.isAuthenticated() && !sec.isExpired() ? (
                    sec.hasAtLeastOneRole(['cyccedit', 'cyccview']) ?
                        <Component {...props} />
                        : <Redirect
                            to={{
                            pathname: "/unauthorized",
                            state: { from: props.location }
                            }}
                        />
                    ) : (
                    <Redirect
                        to={{
                        pathname: "/login",
                        state: { from: props.location }
                        }}
                    />
                    )
                }}
                />)
    }
}

const AppRoutes = () => (
    <Switch>
        <Route path="/" component={AdminRoutes}/>
        <Route path="/unauthorized" component={Unauthorized}/>
        <Redirect path="*" to="/"/>
    </Switch>
)

const AdminRoutes = () => (
    <AdminLayout>
        <div className='page'>
            <PrivateRoute exact path={"/claims/:id"} component={ClaimDetails} />
            <PrivateRoute exact path={"/external-transfers/:id"} component={ExternalTransferDetails} />
            <PrivateRoute exact path={"/provider-payment-cards/:id"} component={ProviderPaymentCards} />
            <PrivateRoute exact path="/claims" component={Claims} />
            <PrivateRoute exact path={"/members"} component={Members}/>
            <PrivateRoute exact path={"/jobs/:id"} component={Jobs}/>
            <PrivateRoute exact path="/accounts/:id" component={AccountDetails} />
            <PrivateRoute exact path="/employers/:id" component={EmployerDetail} />
            <PrivateRoute exact path="/providers/:id" component={Providers}/>
            <PrivateRoute exact path="/quicklookup" component={QuickLookup} />
            <PrivateRoute exact path="/quicklookup/contributions" component={QuickLookupContributions} />
            <PrivateRoute exact path="/quicklookup/employers" component={QuickLookupEmployers} />
            <PrivateRoute exact path="/filemonitor" component={FileMonitor} />
            <PrivateRoute exact path="/filemonitor/employers/:employerId" component={FileMonitorDetails} />
            <PrivateRoute exact path="/" component={Index} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/unauthorized" component={Unauthorized} />
        </div>
    </AdminLayout>
);

export default AppRoutes
