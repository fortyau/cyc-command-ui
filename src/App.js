import React from 'react'
import Reflux from 'reflux'
import { BrowserRouter as Router } from "react-router-dom"
import Header from 'components/Header'
import AppRoutes from './routes'

import Snackbar from 'components/Snackbar'

// Stores --------------------------------
import SnackbarStore from 'stores/SnackbarStore'
import QuickLookupStore from 'stores/QuickLookupStore'
import RecentClaimStore from 'stores/RecentClaimStore'

// Styles -----------------
import 'styles/global.scss'
import 'compositions/navigation/site-nav.scss'

class App extends Reflux.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.stores = [SnackbarStore, RecentClaimStore, QuickLookupStore]
  }

  /*
  * Render snackbar
  */
  renderSnackbar () {
    const {open, message, type} = this.state
    return (
      <Snackbar
        message={message}
        open={open}
        type={type}
      />
    )
  }

  /**
   * Renders the app...
   */
  render() {
    return (
        <Router>
            <div>
                <Header/>
                <AppRoutes />
                {this.renderSnackbar()}
            </div>
        </Router>
    )
  }
}

export default App
