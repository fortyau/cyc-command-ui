import { Promise } from 'es6-promise-polyfill'
import SessionStore from 'stores/SessionStore'
import {snackbarActions} from 'stores/SnackbarStore'

/**
 * Small helper module and entry/exit point for all the HTTP
 * requests made within the application.
 */
const Fetch = {

  /**
   * @params {String} method for request
   * @params {String} endpoint to open a request to
   **/
  open: function(method, endpoint, token) {
    const request = new XMLHttpRequest()

    request.open(method, endpoint)
    request.setRequestHeader('Content-Type', 'application/json')


    try {
      // const tok = token || SessionStore.singleton.state.token
      // request.setRequestHeader('Authorization', `Bearer ${tok}`)
    } catch (e) {
      console.error(e)
    }

    return {request}
  },


  /**
   * @params {String} endpoint to open a request to
   * @params {Object} params to POST
   * @params {Function} loading function to call
   **/
  post: function(endpoint, params, loading) {
    const {request} = this.open('POST', endpoint)

    return new Promise((resolve, reject) => {
      request.onreadystatechange = function () {
        /* eslint-disable default-case, no-fallthrough */
        switch( this.readyState ){
          case 1: { loading && loading() }
          case 4: {
            try {
              const resp = JSON.parse(this.responseText)

              if (this.status >= 200 && this.status < 300) {
                resolve(resp)
              } else {
                snackbarActions.showSnackbar(resp.message)
                reject(resp)
              }

            } catch (e) {
              reject(e, this.responseText)
            }
          }
        }
      }

      request.send(JSON.stringify(params))
    })
  },


  /**
   * @params {String} endpoint to open a request to
   * @params {Object} params to PUT
   * @params {Function} loading function to call
   **/
  put: function(endpoint, params, loading) {
    const {request} = this.open('PUT', endpoint)

    return new Promise((resolve, reject) => {
      request.onreadystatechange = function () {
        /* eslint-disable default-case, no-fallthrough */
        switch( this.readyState ){
          case 1: { loading && loading() }
          case 4: {
            try {
              const resp = JSON.parse(this.responseText)

              if (this.status >= 200 && this.status < 300) {
                resolve(resp)
              } else {
                snackbarActions.showSnackbar(resp.message)
                reject(resp) }
            } catch (e) {
              reject(e, this.responseText)
            }
          }
        }
      }

      request.send(JSON.stringify(params))
    })
  },


  /**
   * @params {String} endpoint to open a request to
   * @params {Function} loading function to call
   **/
  get: function(endpoint, loading, token) {
    const {request} = this.open('GET', endpoint, token)

    return new Promise((resolve, reject) => {
      request.onreadystatechange = function () {
        /* eslint-disable default-case, no-fallthrough */
        switch( this.readyState ){
          case 1: { loading && loading() }
          case 4: {
            if (this.responseText) {
              resolve(JSON.parse(this.responseText))
            } else { resolve(this.responseText) }
          }
        }
      }

      request.send()
    })
  },


  /**
   * @params {String} endpoint to open a request to
   * @params {Function} loading function to call
   **/
  delete: function(endpoint, loading) {
    const {request} = this.open('DELETE', endpoint)

    return new Promise((resolve, reject) => {
      request.onreadystatechange = function () {
        /* eslint-disable default-case, no-fallthrough */
        switch( this.readyState ){
          case 1: { loading && loading() }
          case 4: {
            if (this.responseText) {
              resolve(JSON.parse(this.responseText))
            } else { resolve(this.responseText) }
          }
        }
      }

      request.send()
    })
  }
}

export default Fetch
