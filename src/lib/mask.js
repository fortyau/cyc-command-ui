/*
* Mask date
*/
export function maskDate(oldValue, newValue) {
  let maskedValue = newValue.replace(/[a-zA-Z]/g, '')
  if (newValue.length > 10) return oldValue
  if (oldValue.length > newValue.length) return maskedValue
  switch (newValue.length) {
    case 2:
      maskedValue = newValue + '/'
      break
    case 3:
      if (newValue[2] !== '/') {
        maskedValue = `${newValue.slice(0, 2)}/${newValue[2]}`
      }
      break
    case 5:
      maskedValue = newValue + '/'
      break
    case 6:
      if (newValue[5] !== '/') {
        maskedValue = `${newValue.slice(0, 5)}/${newValue[5]}`
      }
      break
    default:
      break
  }
  return maskedValue
}
