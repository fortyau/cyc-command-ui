/**
 * Simple utils object...mix as you wish...
 */
const Utils = {
  String: {
    toTitleCase: (str) => {
      return str
              .replace(/_/g, ' ')
              .replace(/\w\S*/g, (txt) =>
                txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase() )
    },

    toCleanSlug: (str, glue='-') => {
      return str.toLowerCase().replace(/ /gi, glue)
    },

    toClassName: (str) => {
      return str.toLowerCase().replace(/ /gi, '-').replace(/_/gi, '-')
    }
  },

  Bytes: {
    toSizeString: (bytes) => {
      const MB = 1024;
      let sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
      if(!bytes) return '? Bytes';
      if (bytes === 0) return '0 Bytes';
      let i = parseInt(Math.floor(Math.log(bytes) / Math.log(MB)), 10);
      return `${(Math.round((bytes / Math.pow(MB, i) * 100)) / 100).toLocaleString()} ${sizes[i]}`;
    }
  },

  Number: {
    toCurrency: (n) => {
      const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD'
      });

      return formatter.format(n).replace(/\.\d{2}/g, '');
    }
  }
}

export default Utils
