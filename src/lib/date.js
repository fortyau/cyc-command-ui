/*
* Map date to form
*/
export function mapDateToForm (date) {
  if (date) {
    const dateParts = date.substring(0, 10).split('-')
    return `${dateParts[1]}/${dateParts[2]}/${dateParts[0]}`
  }
}

/*
* Map date to form with time
*/
export function mapDateToFormWithTime (date) {
  if (date) {
    const dateParts = date.substring(0, 10).split('-');
    let timeParts = '';

    if(date.includes('T') && date.includes('Z')) {
      timeParts = date.split('T')[1].split('Z')[0];
    }

    return `${dateParts[1]}/${dateParts[2]}/${dateParts[0]} ${timeParts}`
  }
}

/*
* Map form to date
*/
export function mapFormDateToDate (date) {
  if (date) {
    const dateParts = date.split('/')
    return `${dateParts[2]}-${dateParts[0]}-${dateParts[1]} 00:00:00`
  }
}