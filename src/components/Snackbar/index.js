import React from 'react'
import PropTypes from 'prop-types'
import './snackbar.scss'

class Snackbar extends React.Component {
  render () {
    const {open, message, type} = this.props
    const classes = ['snackbar']
    if (open) {
      classes.push('show')
      classes.push(type)
    }

    return (
      <div className={classes.join(' ')}>{message}</div>
    )
  }
}

Snackbar.propTypes = {
  message: PropTypes.string,
  open: PropTypes.bool,
  type: PropTypes.string
}

export default Snackbar
