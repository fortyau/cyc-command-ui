import React from 'react'

import 'components/SearchBox/SearchBox.scss'

class SearchBox extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
        value: null
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.onSubmit(this.state.value);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
    this.props.onChange(event.target.value);
  }

  getLabel() {
    return (this.props.label || '');
  }

  render () {
    return (
          <form onSubmit={this.handleSubmit}>
            <label className="search-box-label">{this.getLabel()}</label>
            <br/>
            <input
              className="search-box"
              style={{width: 'auto'}}
              value={this.props.value}
              onChange={this.handleChange}
              placeholder={this.props.placeholder}
              type="text" />
            <button
              style={{width: 'auto', borderRadius: "0px 3px 3px 0px" }}
              className='button button-primary search-box'
              type='submit'>
              <i className='material-icons'>search</i>
              </button>
          </form>
    )
  }
}

export default SearchBox
