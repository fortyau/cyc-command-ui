import React from 'react'
import Reflux from 'reflux'

import 'styles/global.scss'

class PageTitle extends Reflux.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		const {className} = this.props;

		return (
			<div className={className} style={{fontSize: '28px'}}>
				{this.props.children}
				{this.props.value}
			</div>
		)
	}
}

export default PageTitle