import React from 'react'
// eslint-disable-next-line
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom"

import 'components/DrilldownTable/DrilldownTable.scss'

class DrilldownTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = { paginatedData: null };
  }

  componentWillReceiveProps() {
    if(this.props.source && this.props.source.length > 0 && !this.state.paginatedData) {
      this.setState({ paginatedData: this.paginateData() });
    }
  }

  paginateData() {
    const paginatedData = {
      data: [],
      currentView: {
        page: 1,
        rows: {
          from: 1,
          to: -1
        }
      },
      pages: {
        from: 1,
        to: -1
      }
    };

    if(this.paginationEnabled()) {
      paginatedData.pages.to = Math.ceil(this.props.source.length / this.props.pageSize);
      paginatedData.currentView.rows.to = this.props.pageSize;

      for(var c=0; c<paginatedData.pages.to; c++) {
        var rowFrom = (c * this.props.pageSize);
        var rowTo = (rowFrom + this.props.pageSize);
        paginatedData.data[c + 1] = this.props.source.slice(rowFrom, rowTo);
      }
    }

    return paginatedData;
  }

  paginationEnabled() {
    return this.props.pageSize && this.props.pageSize > 0;
  }

  getTableData() {
    let data = [];

    if(this.paginationEnabled()) {
      if(this.state.paginatedData) {
        if(this.state.paginatedData.data.length > 0) {
          data = this.state.paginatedData.data[this.state.paginatedData.currentView.page];
        }
      } else {
        const paginatedData = this.paginateData();
        if(paginatedData.data.length > 0) {
          data = paginatedData.data[paginatedData.currentView.page];
        }
      }
    } else {
      data = this.props.source;
    }

    return data;
  }

  getText(sourceItem, columnConfig) {
    if (columnConfig.getText) {
        return columnConfig.getText(sourceItem);
    }

    return sourceItem[columnConfig.property];
  }

  getRowHightlightColor(row) {
    var color = '';

    if(this.props.rowHighlightColor && this.props.rowHighlightCondition) {
      if(this.props.rowHighlightCondition(row)) {
        color = this.props.rowHighlightColor;
      }
    }

    return color;
  }

  handlePageNavFirst() {
    const paginatedData = this.state.paginatedData;
    paginatedData.currentView.page = 1;
    this.setState({
      paginatedData
    });
  }

  handlePageNavPrevious() {
    const paginatedData = this.state.paginatedData;
    const nextPage = paginatedData.currentView.page - 1;

    if(nextPage >= 1) {
      paginatedData.currentView.page = nextPage;
      this.setState({
        paginatedData
      });
    }
  }

  handlePageNavNext() {
    const paginatedData = this.state.paginatedData;
    const nextPage = paginatedData.currentView.page + 1;
    const totalPages = paginatedData.pages.to;

    if(nextPage <= totalPages) {
      paginatedData.currentView.page = nextPage;
      this.setState({
        paginatedData
      });
    }
  }

  handlePageNavLast() {
    const paginatedData = this.state.paginatedData;
    paginatedData.currentView.page = paginatedData.pages.to;
    this.setState({
      paginatedData
    });
  }

  renderPageNavButton(icon, onClick) {
    return (
      <button
        className='button-nav button-primary'
        onClick={onClick}
        type='submit'>
          <i className='material-icons'>{icon}</i>
      </button>
    );
  }

  renderTableNav() {
    return (
      <div className='page-nav'>
        {this.renderPageNavButton("first_page", this.handlePageNavFirst.bind(this))}
        {this.renderPageNavButton("chevron_left", this.handlePageNavPrevious.bind(this))}
        <div className='current-nav'>
        {this.state.paginatedData.currentView.page} of {this.state.paginatedData.pages.to}
        </div>
        {this.renderPageNavButton("chevron_right", this.handlePageNavNext.bind(this))}
        {this.renderPageNavButton("last_page", this.handlePageNavLast.bind(this))}
      </div>
    );
  }

  render () {
    return (
        <div>
            <table className={this.props.tableClass}>
                <thead>
                    <tr>
                        {this.props.cols.map((c, ci) => <td key={'ddtableheader-' + ci}>{c.header.toUpperCase()}</td>)}
                    </tr>
                </thead>
                <tbody>
                    {this.getTableData().map(
                        (sourceItem, sourceItemIndex) =>
                        <tr key={'ddtablerow-' + sourceItemIndex } style={{background: this.getRowHightlightColor(sourceItem)}}>
                        {this.props.cols.map(
                            (c, ci) =>
                            <td key={'ddtabledata-' + sourceItemIndex + '-' + ci }>
                                {c.isLink ?
                                    <NavLink className='a-link' to={this.props.buildLink(sourceItem)}>{this.getText(sourceItem, c)}</NavLink>
                                    : c.clickable ?
                                    <a className='a-link' onClick={() => c.onClick(sourceItem)}>{this.getText(sourceItem, c)}</a>
                                    : this.getText(sourceItem, c)}
                            </td>)}
                        </tr>)}
                </tbody>
            </table>
            <div className="bottom-border" />
            {this.paginationEnabled() && this.state.paginatedData && this.state.paginatedData.pages.to > 1 ? this.renderTableNav() : null}
        </div>
    )
  }
}

export default DrilldownTable
