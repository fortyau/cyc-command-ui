import React from 'react';
// eslint-disable-next-line
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

import 'components/MultiPageTable/MultiPageTable.scss'

class MultiPageTable extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
      preFetch: true,
      currentPage: 0,
      pageCount: 0,
      data: []
    };
  }

  componentDidMount() {
    this.search();
  }

  search() {
    this.getPageData(1, true);
  }

  getText(sourceItem, columnConfig) {
    if (columnConfig.getText) {
        return columnConfig.getText(sourceItem);
    }

    return sourceItem[columnConfig.property];
  }

  getRowHightlightColor(row) {
    let color = '';

    if(this.props.rowHighlightColor && this.props.rowHighlightCondition) {
      if(this.props.rowHighlightCondition(row)) {
        color = this.props.rowHighlightColor;
      }
    }

    return color;
  }

  getPageData(requestedPage, force) {
    const {pageCount, preFetch} = this.state;

    if(force || preFetch || (requestedPage >= 1 && requestedPage <= pageCount)) {
      this.setState({ data:[] }); // Force more of visual reload
      this.props.onPageRequest(this.props.pageSize, requestedPage, preFetch)
          .then(data => {
              this.setState({
                  preFetch: false,
                  currentPage: requestedPage,
                  pageCount: data.pageInfo.pages,
                  data: data.items
                });

                return { pageCount: data.pageInfo.pages, items: data.items };
          })
          .then(data => this.props.onPageRequestComplete(data.pageCount, data.items));
      }
  }

  renderPageNavButton(icon, onClick) {
    return (
      <button
        className='button-nav button-primary'
        onClick={onClick}
        type='submit'>
          <i className='material-icons'>{icon}</i>
      </button>
    );
  }

  renderTableNav() {
    return (
      <div className='page-nav'>
        {this.renderPageNavButton("first_page", this.getPageData.bind(this, 1, false))}
        {this.renderPageNavButton("chevron_left", this.getPageData.bind(this, this.state.currentPage - 1, false))}
        <div className='current-nav'>
        {this.state.currentPage} of {this.state.pageCount}
        </div>
        {this.renderPageNavButton("chevron_right", this.getPageData.bind(this, this.state.currentPage + 1, false))}
        {this.renderPageNavButton("last_page", this.getPageData.bind(this, this.state.pageCount, false))}
      </div>
    );
  }

  render () {
    return (
        <div>
            <table>
                <thead>
                    <tr>
                        {this.props.cols.map((c, ci) => <td key={'ddtableheader-' + ci}>{c.header.toUpperCase()}</td>)}
                    </tr>
                </thead>
                <tbody>
                    {
                      this.state.data.map(
                        (sourceItem, sourceItemIndex) =>
                        <tr key={'ddtablerow-' + sourceItemIndex } style={{background: this.getRowHightlightColor(sourceItem)}}>
                        {this.props.cols.map(
                            (c, ci) =>
                            <td key={'ddtabledata-' + sourceItemIndex + '-' + ci }>
                                {c.isLink ?
                                    <NavLink className='a-link' to={this.props.buildLink(sourceItem)}>{this.getText(sourceItem, c)}</NavLink>
                                    : c.clickable ?
                                    <a className='a-link' onClick={() => c.onClick(sourceItem)}>{this.getText(sourceItem, c)}</a>
                                    : this.getText(sourceItem, c)}
                            </td>)}
                        </tr>)
                      }
                </tbody>
            </table>
            <div className="bottom-border" />
            { this.state.data.length > 0 ? this.renderTableNav() : null }
        </div>
    )
  }
}

export default MultiPageTable
