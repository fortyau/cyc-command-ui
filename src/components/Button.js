import React, { Component } from 'react'

// Styles ------------
import './button.scss'


/**
 * TODO Write documentation
 */
class Button extends Component {

  static defaultProps = {
    className: '',
    disabled: false,
    loading: false,
    type: 'button',
    events: {}
  }

  renderChildren() {
    const { children, loading } = this.props

    if (loading) {
      return (<span role="img" aria-label="coffee">&#9749;</span>)
    } else {
      return children
    }
  }


  render() {
    const { className, events, type } = this.props

    let classNames = ['button', `button-${type}`]
    classNames.push(className)

    return (
      <button {...events} className={classNames.filter(a => a).join(' ')}>
        {this.renderChildren()}
      </button>
    )
  }
}

export default Button
