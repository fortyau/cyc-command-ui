import React from 'react'
// eslint-disable-next-line
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom"

// Styles --------------------------------
import 'components/NumberField/number-field.scss'

class NumberField extends React.Component {
  constructor (props) {
    super (props);
    this.state = {touched: false};
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({touched: true});
    event.preventDefault();
    if (this.props.onChange)
    {
      this.props.onChange(event.target.value);
    }
  }

  render () {
    const {label, name, value, link, editable, onChange, ...other} = this.props
    return (
      <div className="margin-top">
        <label className="text-field-label">{label.toUpperCase()}</label>
        <div className="text-field" id={name}>
          {link ?
            <NavLink className='a-link' to={link.url}>{link.text}</NavLink>
            : editable ? 
            <input type="number" className={`margin-right${this.state.touched ? ' touched' : ''}`}
              style={{width: 'auto'}} onChange={this.handleChange} value={value || 0} {...other} /> 
            : <span>{this.getText(value)}</span>}
        </div>
      </div>
    )
  }
}

export default NumberField
