import React from 'react'
import Reflux from 'reflux'

import 'styles/global.scss'

class SectionTitle extends Reflux.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  renderActions() {
    // WIP - needs callback stuff
    return (
      <span className="margin-left" style={{fontWeight: 'normal'}}>
        {this.props.actions && this.props.actions.length > 0 ? 
        this.props.actions.map(a => <a className="button-inline margin-right">{a}</a>)
        : null}
      </span>
    )
  }

  render() {
    return (
        <div style={{fontSize: '16px', fontWeight: 'bold', marginTop: '2em'}}>
            {this.props.value.toUpperCase()}
            {this.renderActions()}
        </div>
    )
  }
}

export default SectionTitle