import React from 'react'
import PropTypes from 'prop-types'
import Card from '../Card'
import './modal.scss'
import SectionTitle from 'components/SectionTitle'

class Modal extends React.Component {
  renderHeader () {
    if (this.props.header) {
      return (
        <div className='modal-header'>
          <SectionTitle value={this.props.header} />
          {this.renderCloseButton()}
        </div>
      )
    }
  }

  renderCloseButton () {
    if (this.props.showClose) {
      return (
        <button
            className="button button-close"
            style={{width: 'auto'}}
            type='submit'
            onClick={() => this.props.onClose()}>
            <i className='material-icons'>close</i>
          </button>
      )
    }
  }

  onClose() {
    if (this.props.onClose) {
      this.props.onClose()
    }
  }

  render () {
    return (
      <div onClick={() => this.onClose()} className="overlay-style">
        <Card className="modal" onClick={(event) => { event.stopPropagation() }}>
          {this.renderHeader()}
          {this.props.children}
        </Card>
      </div>
    )
  }
}

Modal.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func
}

export default Modal
