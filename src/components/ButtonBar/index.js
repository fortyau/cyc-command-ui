import React from 'react'
import Reflux from 'reflux'

import 'styles/global.scss'

class ButtonBar extends Reflux.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    renderActions() {
        return (
            <span >
                {this.props.actions.map(a => <button className="button button-inline button-edit margin-right" key={"button-" + a} onClick={(e) => this.handleClick(e, a)}>{a}</button>)}
            </span>
        )
    }

    renderStep2() {
        return (
            <div>
                <button className="button button-secondary button-fixed button-inline margin-right" type="button" onClick={(e) => this.handleClick(e, "cancel")}>Cancel</button>
                <button className="button button-primary button-fixed button-inline" type="submit">Save</button>
            </div>
        )
    }

    handleClick(e, action) {
        this.props.onClick(action)
        e.preventDefault();
    }

    render() {
        return (
            <div className="margin-top">
                {!this.props.toggle ?
                    this.renderActions()
                    : this.renderStep2()}
            </div>
        )
    }
}

export default ButtonBar