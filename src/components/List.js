import React, { Component } from 'react'

class List extends Component {

  static defaultProps = {
    items: [],
    nouns: ['item', 'items']
  }


  /**
   * TODO Write documentation...
   */
  renderActions(item){
    if ( item.actions ) {
      return (
        <ul className="actions">
          {item.actions.map((action) => {
            return action
          })}
        </ul>
      )

    } else return null
  }


  /**
   * TODO Write documentation...
   */
  renderItem(item, idx) {
    const classNames = ['item']
    classNames.push(item.actions ? 'with-actions' : null)

    return (
      <li key={`item-${idx}`} className={classNames.filter(a => a).join(' ')}>
        {item.label}
        {this.renderActions(item)}
      </li>
    )
  }


  /**
   * TODO Write documentation...
   */
  render() {
    const { items, nouns } = this.props

    if ( items && items.length === 0 ) {

      return (
        <span className='no-items'>No {nouns[1]} found.</span>
      )
    } else {

      return (
        <ul>
          {items.map((item, idx) => {
            return this.renderItem(item, idx)
          })}
        </ul>
      )
    }
  }
}

export default List
