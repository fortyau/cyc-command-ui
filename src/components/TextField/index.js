import React from 'react'
import PropTypes from 'prop-types'
// eslint-disable-next-line
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom"

// Styles --------------------------------
import 'components/TextField/text-field.scss'

class TextField extends React.Component {
  constructor (props) {
    super (props);
    this.state = {touched: false};
    this.handleChange = this.handleChange.bind(this);
    this.getText = this.getText.bind(this);
  }

  handleChange(event) {
    this.setState({touched: true});
    event.preventDefault();
    if (this.props.onChange)
    {
      this.props.onChange(event.target.value);
    }
  }

  getText(src) {
    if (this.props.formatter) {
      return this.props.formatter(src);
    }
    return src;
  }

  render () {
    const {label, name, value} = this.props
    return (
      <div className="margin-top">
        <label className="text-field-label">{label.toUpperCase()}</label>
        <div className="text-field" id={name}>
            {this.props.editable ? 
                <input type={this.props.type || 'text'} className={`margin-right${this.state.touched ? ' touched' : ''}`} value={this.props.value} 
                    style={{width: 'auto'}} onChange={this.handleChange} size={this.props.size || 20} required={this.props.required} 
                    maxLength={this.props.maxLength} minLength={this.props.minLength} pattern={this.props.pattern} autoFocus={this.props.autoFocus || false} /> 
                : this.props.link ? 
                    <NavLink className='a-link' to={this.props.link.url}>{this.props.link.text}</NavLink>
                    : <span>{this.getText(value)}</span>}
        </div>
      </div>
    )
  }
}

TextField.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  editable: PropTypes.bool,
  required: PropTypes.bool,
  size: PropTypes.number,
  onChange: PropTypes.func,
  maxLength: PropTypes.number,
  minLength: PropTypes.number,
  pattern: PropTypes.string,
  link: PropTypes.object,
  type: PropTypes.string
}

export default TextField
