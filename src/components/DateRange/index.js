import React from 'react'
import moment from 'moment'
import DateField from 'components/DateField'

import './DateRange.scss';

class DateRange extends React.Component {
    reset() {
      this.refs.dateFrom.clear();
      this.refs.dateTo.clear();
    }

    toDate(value) {
      let date = value;

      if(value) {
        date = moment(value, 'YYYY-MM-DD');
      }

      return date;
    }

    handleDateFromChange(date) {
      this.props.onDateFromChange(this.toDate(date));
    }

    handleDateToChange(date) {
      this.props.onDateToChange(this.toDate(date));
    }

    render() {
      return (
          <div className={'date-range'}>
              <div className={'date-range-label'}>{this.props.label}</div>
              <DateField className={'date-range-field'}
                    containerDisplay='inline-block'
                     ref="dateFrom"
                     name="dateFrom"
                     type="date"
                     inline={true}
                     label={this.props.dateFromLabel || "Start Date" }
                     onChange={this.handleDateFromChange.bind(this)}/>
              <DateField className={'date-range-field'}
                    containerDisplay='inline-block'
                     ref="dateTo"
                     name="dateTo"
                     type="date"
                     inline={true}
                     label={this.props.dateToLabel || "End Date" }
                     onChange={this.handleDateToChange.bind(this)}/>
              {this.props.children}
          </div>
      );
    }
}

export default DateRange
