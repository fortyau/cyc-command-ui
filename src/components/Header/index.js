import React from 'react'
import Reflux from 'reflux'
import { Link, NavLink } from 'react-router-dom'

import 'styles/global.scss'
import './header.scss'

import logo from 'assets/images/CYC-Logo_header.svg'
import Security from 'data/security'

const sizes = [
    'big',
    'small',
    'tiny'
]

const routes = [
    { big: 'Batch Monitor', small: 'Bat Mon', tiny: 'BM', url: '' },
    { big: 'File Monitor', small: 'Fil Mon', tiny: 'FL', url: '/filemonitor' },
    { big: 'Batch Configuration', small: 'Bat Con', tiny: 'BC', url: '' },
    { big: 'File Analyzer', small: 'Fil Anyl', tiny: 'FA', url: '' },
    { big: 'Claims', small: 'Clm', tiny: 'CL', url: '/claims' },
    { big: 'Cards', small: 'Crd', tiny: 'CD', url: '' },
    { big: 'Bank Ops', small: 'Bnk Op', tiny: 'BO', url: '' },
    { big: 'Implementation', small: 'Impl', tiny: 'I', url: '' },
    { big: 'Accounts and Enrollment', small: 'Accts & Enr', tiny: 'A&E', url: '/members' },
    { big: 'Quick Lookup Tool', small: 'Quick Look', tiny: 'QLT', url: '/quicklookup' }
]

class Header extends Reflux.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.logout = this.logout.bind(this);
  }

  logout() {
    new Security().logout();
    window.location = '/login';
  }

  render() {
    return (
        <div>
            <div className="banner-header" style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-end'}}>
                <Link to="/"><img src={logo} className="margin-right" alt="Connect your Care" style={{marginTop: "10px"}}/></Link>
                <span className="rectangle app-title" style={{marginRight: 'auto'}}>
                    CYC COMMAND
                </span>
                <a className='a-link white' onClick={this.logout}>Logout</a>
            </div>

            {sizes.map((size, idx1) =>
                <ul className={`menu menu-${size}`} key={idx1}>
                    {routes.map((route, idx2) =>
                        <li className="menu-item" key={idx1 + idx2}>
                            {route.url ? 
                                <NavLink to={route.url}>{route[size]}</NavLink>
                                : <a>{route[size]}</a>}
                        </li>)}
                </ul>
            )}
        </div>
    )
  }
}

export default Header
