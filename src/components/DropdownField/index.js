import React from 'react'

import './DropdownField.scss'

class DropdownField extends React.Component {
  constructor (props) {
    super (props);
    this.state = {touched: false};
    this.handleChange = this.handleChange.bind(this);
    this.getValue = this.getValue.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value, touched: true});
    if (this.props.onChange) {
        const val = this.props.options.find(o => {
            var x = String(this.getValue(o)) === String(event.target.value);
            return x;
        });
        this.props.onChange(val);
    }
    event.preventDefault();
  }

  getOptions() {
    if (this.props.groupByPath) {
        return this.getGroupedOptions();
    }
    else {
        return this.props.options.map(o => <option key={this.getKey(o)} value={this.getValue(o)}>{this.getText(o)}</option>);
    }
  }

  getGroupedOptions() {
    let groups = [];
    let lastGroupName = null;
    let currentGroup = [];
    this.props.options.forEach(o => {
        let groupName = o[this.props.groupByPath]
        if (!groupName) {
            groupName = "{Unnamed Category}"
        }
        if (lastGroupName !== groupName) {
            lastGroupName = groupName;
            currentGroup = [];
            groups.push({
                name: lastGroupName,
                items: currentGroup
            });
        }
        currentGroup.push(o);
    })
    return groups.map(g => 
        <optgroup label={g.name} key={g.name}>
            {g.items.map(o => <option value={this.getValue(o)} key={this.getKey(o)}>{this.getText(o)}</option>)}
        </optgroup>);
  }

  getValue(option) {
    if (this.props.valuePath) {
        return option[this.props.valuePath];
    }
    return option;
  }

  getText(option) {
    let text = '';
    if (option) {
        if (this.props.displayPath) {
            text = option[this.props.displayPath];
        }
        else {
            text = option;
        }

        if (this.props.formatter) {
            text = this.props.formatter(text);
        }
    }
    return text;
  }

  getKey(option) {
      if (this.props.keyPath) {
          return option[this.props.keyPath];
      }
      if (this.props.valuePath) {
        return option[this.props.valuePath];
      }
      return this.getText(option);
  }

  render () {
    const {label, name, value} = this.props;
    return (
      <div className="margin-top">
        <label className="dropdown-field-label">{label.toUpperCase()}</label>
        <div className="dropdown-field" id={name}>
            <select className="dropdown" onChange={this.handleChange} value={value}>
                {this.getOptions()}
            </select>
        </div>
      </div>
    )
  }
}

export default DropdownField
