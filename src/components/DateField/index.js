import React from 'react'
import PropTypes from 'prop-types'

// Styles --------------------------------
import 'components/DateField/date-field.scss'

class DateField extends React.Component {
    constructor (props) {
        super (props);
        this.state = {touched: false};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value, touched: true});
        event.preventDefault();
        if (this.props.onChange)
        {
            this.props.onChange(event.target.value);
        }
    }

    clear() {
      this.refs.input.value = "";
    }

    render () {
        const {label, name, value} = this.props
        return (
            <div className="margin-top" style={{display: this.props.containerDisplay || 'block'}}>
                <label className={this.props.inline ? "date-field-label-inline" : "date-field-label"}>{label.toUpperCase()}</label>
                <div className={this.props.inline ? "date-field-inline" : "date-field"} id={name}>
                    <input type='date' ref="input" className={`margin-right${this.state.touched ? ' touched' : ''}`} value={value}
                        style={{width: 'auto'}} onChange={this.handleChange} required={this.props.required} />
                </div>
            </div>
        )
    }
}

DateField.propTypes = {
    label: PropTypes.string,
    name: PropTypes.string,
    required: PropTypes.bool,
    onChange: PropTypes.func
}

export default DateField
