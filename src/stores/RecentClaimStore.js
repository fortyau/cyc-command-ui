import Reflux from 'reflux'

export const recentClaimActions = Reflux.createActions([
	'setRecentClaim'
])

class RecentClaimStore extends Reflux.Store {
	constructor() {
		super()
		this.state = this.initializeState()
		this.listenables = recentClaimActions
	}

	initializeState () {
		return {
			claimId: null
		}
	}

	onSetRecentClaim(claimId) {
		this.setState({claimId: claimId})
	}
}

export default RecentClaimStore