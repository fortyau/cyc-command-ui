import Reflux from 'reflux'

export const sessionActions = Reflux.createActions([
    'setAuthToken',
    'setExpires',
    'setRefreshExpires',
    'setRefreshToken',
    'setRoles'
])

class SessionStore extends Reflux.Store
{
    constructor() {
        super()
        this.state = this.initializeState()
        this.listenables = sessionActions
    }

    /*
    * Initialize the state
    */
    initializeState () {
        return {
            authToken: null,
            refreshToken: null,
            roles: [],
            expires: null,
            refreshExpires: null
        }
    }

    /*
    * Set Auth token.
    *
    * @params [token]. Auth token.
    */
    onSetAuthToken (token) {
        this.setState({authToken: token})
    }

    /*
    * Set refresh token.
    *
    * @params [token]. Refresh token.
    */
    onSetRefreshToken (token) {
        this.setState({refreshToken: token})
    }

    /*
    * Set user roles.
    *
    * @params [roles]. User roles.
    */
    onSetRoles (roles) {
        this.setState({roles})
    }

    /*
    * Set auth token expires
    *
    * @params [number]. Auth token expiration.
    */
    onSetExpires (expires) {
        this.setState({expires})
    }

    onSetRefreshExpires (refreshExpires) {
        this.setState({refreshExpires})
    }
}

const sessionStore = new SessionStore ()

export default sessionStore
