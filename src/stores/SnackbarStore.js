import Reflux from 'reflux'

export const snackbarActions = Reflux.createActions([
    'showSnackbar'
])

class SnackbarStore extends Reflux.Store
{
  constructor() {
    super()
    this.state = this.initializeState()
    this.listenables = snackbarActions
  }

  /*
  * Initialize the state
  */
  initializeState () {
    return {
      message: null,
      open: false,
      type: null
    }
  }

  /*
  * Open snackbar
  *
  * @params [String] message. Message to show
  * @params [String] type. Message type. info | warn | error
  */
  onShowSnackbar (message, type = 'error') {
    this.setState({open: true, message, type})

    setTimeout(() => {
      this.setState(this.initializeState())
    }, 3000)
  }
}

export default SnackbarStore
