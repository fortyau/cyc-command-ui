import Reflux from 'reflux'

export const quickLookupStoreActions = Reflux.createActions([
	'setSearch',
	'clearSearch'
])

class QuickLookupStore extends Reflux.Store {
	constructor() {
		super();
		this.state = this.initializeState();
		this.listenables = quickLookupStoreActions;
	}

	initializeState () {
		return {
			search: {
				type: null,
				text: null
			}
		};
	}

	onSetSearch(searchType, searchText) {
		this.setState({
			search: {
				type: searchType,
				text: searchText
			}
		});
	}

	onClearSearch() {
		this.setState({
			search: {
				type: null,
				text: null
			}
		});
	}
}

export default QuickLookupStore
