class ExternalTransferPaymentViewModel {
    constructor(externalTransfer) {
        const reimbursementType = this.getReimbursementType(externalTransfer);
        this.linkText = this.linkText(externalTransfer, reimbursementType);
        this.linkUrl = `/external-transfers/${externalTransfer.id}`;
        this.reimbursementMethod = reimbursementType;
        this.paidDate = externalTransfer.paidDate;
        this.clearedDate = externalTransfer.clearedDate;
        this.paidTo = this.paidTo(externalTransfer);
        this.address = this.address(externalTransfer);
        this.paymentStatus = externalTransfer.paymentStatus;
    }

    linkText(externalTransfer, reimbursementType) {
        var num = externalTransfer.journalId;
    
        if (reimbursementType === "Check") {
          num = externalTransfer.checkNumber;
        }
    
        return `${reimbursementType} #${num}`;
    }

    paidTo(et) {
        return et.provider ? et.provider.name : null;
    }

    address(et) {
        if (!et.provider) {
            return null;
        }
        const prov = et.provider;
        return `${prov.address1} ${prov.city} ${prov.state},${prov.zipCode}`;
    }

    getReimbursementType(x) {
        if (x.typeId === 364) {
          return "Check";
        }
        
        if (x.typeId === 363) {
          return "ACH";
        }
    
        return "UNK";
    }
}

export default ExternalTransferPaymentViewModel;