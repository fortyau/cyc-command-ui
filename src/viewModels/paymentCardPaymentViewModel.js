class PaymentCardPaymentViewModel {
    constructor(paymentCard) {
        this.linkText = `${paymentCard.cardNumber}`;
        this.linkUrl = `/provider-payment-cards/${paymentCard.id}`;
        this.reimbursementMethod = 'Provider Payment Card';
        this.paidDate = `${paymentCard.issuedDate}`;
        this.paymentStatus = this.paymentStatus[paymentCard.cardStatus];
    }

    paymentStatus = {
        2962: "Deleted",
        2963: "Activated",
        130003: "Recycled",
        2700: "Unsent",
        20156: "Voided",
        2960: "To be deleted"
    }
}

export default PaymentCardPaymentViewModel;