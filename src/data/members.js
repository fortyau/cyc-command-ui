import ApiClient from './apiClient';

class MembersData {
	getMembers(form) {
		return new ApiClient().fetchWithConfig(`/members/search`, "POST", form)
	}

}

export default MembersData