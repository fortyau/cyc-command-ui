import ApiClient from './apiClient';

class ClaimStatusData {
	getAll() {
		return new ApiClient().fetchWithConfig(`/claim-statuses`, "GET")
	}
}

export default ClaimStatusData