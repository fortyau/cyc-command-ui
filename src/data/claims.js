import ApiClient from './apiClient';

class ClaimsData {
    getClaims(ids, ssn) {
        return new ApiClient().fetchWithConfig(`/claims/search`, "POST", {"ssn": ssn, "claimIds": ids.split(',')});
    }

    getClaimDetails(id) {
        return new ApiClient().fetchWithConfig(`/claims/${id}`, "GET");
    }

    getClaimsRelatedToExternalTransfer(externalTransferId) {
        return new ApiClient().fetchWithConfig(`/external-transfers/${externalTransferId}/related-claims`, "GET");
    }

    getClaimsRelatedToProviderPaymentCard(providerPaymentCardId) {
        return new ApiClient().fetchWithConfig(`/provider-payment-cards/${providerPaymentCardId}/related-claims`, "GET")
    }

    getClaimsRelatedToProvider(providerId) {
        return new ApiClient().fetchWithConfig(`/providers/${providerId}/related-claims`)
    }

    approveClaim(claimId, body) {
        return new ApiClient().fetchWithConfig(`/claims/${claimId}/status`, "PUT", body)
    }

    update(claimId, changes) {
        return new ApiClient().fetchWithConfig(`/claims/${claimId}`, "PATCH", changes);
    }
}

export default ClaimsData