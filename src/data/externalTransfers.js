import ApiClient from './apiClient';

class ExternalTransfersData {
    getExternalTransfer(id) {
        return new ApiClient().fetchWithConfig(`/external-transfers/${id}`, "GET");
    }

    voidOrReissueExternalTransfer(extId, issueType, rejectReason) {
        const body = {
            'issueType': issueType,
            'rejectReason': rejectReason
        }
        return new ApiClient().fetchWithConfig(`/external-transfers/${extId}/status`, 'PUT', body);
    }

    getExternalTransfersForClaim(claimId) {
        return new ApiClient().fetchWithConfig(`/claims/${claimId}/external-transfers`, "GET");
    }
}

export default ExternalTransfersData