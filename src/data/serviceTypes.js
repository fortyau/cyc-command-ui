import ApiClient from './apiClient';

class ServiceTypesData {
	getAll() {
		return new ApiClient().fetchWithConfig(`/service-types`, "GET")
	}
}

export default ServiceTypesData