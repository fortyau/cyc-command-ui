import ApiClient from './apiClient'

class Enrollments {
    sendEnrollmentUMBHSAB(oid) {
        return new ApiClient().fetchWithConfig(`/enrollments/${oid}/send-enrollment-UMBHSAB`, "PUT")
    }

    sendEnrollmentBNY(body) {
        return new ApiClient().fetchWithConfig(`/enrollments/send-enrollment-BNY`, "PUT", body)
    }
}

export default Enrollments