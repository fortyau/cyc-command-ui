import ApiClient from './apiClient';

class ProvidersData {
    saveAddress(providerId, address) {
        return new ApiClient().fetchWithConfig(`/providers/${providerId}/address`, "PUT", address);
    }

    denyPamentCards(providerId) {
        return new ApiClient().fetchWithConfig(`/providers/${providerId}/deny-payment-cards`, "PUT", {denyPaymentCards: true})
    }

    getPaymentCards(providerId) {
        return new ApiClient().fetchWithConfig(`/providers/${providerId}/provider-payment-cards`, "GET")
    }

    getExternalTransfers(providerId) {
        return new ApiClient().fetchWithConfig(`/providers/${providerId}/external-transfers`, "GET")
    }

    getProvider(providerId) {
        return new ApiClient().fetchWithConfig(`/providers/${providerId}`, "GET")
    }
}

export default ProvidersData