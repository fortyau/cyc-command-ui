import 'isomorphic-fetch'

class AuthData {
    login(user, pass) {
        const config = {
            mode: 'cors',
            method: 'POST',
            body: JSON.stringify({
                "username": user,
                "password": pass
            }),
            headers: {
                "Content-Type": "application/json",
            }
        };
        
        return fetch(`${process.env.REACT_APP_AUTHSVCBASEURL}/users/authenticate`, config);
    }

    refresh(token) {
        const config = {
            mode: 'cors',
            method: 'POST',
            body: JSON.stringify({
                "refresh_token": token
            }),
            headers: {
                "Content-Type": "application/json",
            }
        };

        return fetch(`${process.env.REACT_APP_AUTHSVCBASEURL}/jwt/refresh`, config);
    }
}

export default AuthData
