import ApiClient from './apiClient';

class AccountsData {
	getAccount(id) {
		return new ApiClient().fetchWithConfig(`/accounts/${id}`, "GET")
	}

	clearAccountNumber(id) {
		return new ApiClient().fetchWithConfig(`/accounts/${id}/clearAccountNumber`, "POST")
	}
}

export default AccountsData