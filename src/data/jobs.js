import ApiClient from './apiClient';

class JobsData {

	getAccountDetails(id) {
		return new ApiClient().fetchWithConfig(`/jobs/${id}/accounts`, "GET")
	}

	getMemberDetails(id) {
		return new ApiClient().fetchWithConfig(`/jobs/${id}/member`, "GET")
	}

	purgeJob(id) {
		return new ApiClient().fetchWithConfig(`/jobs/${id}/purge`, "POST")
	}

	getJobs(ssn) {
			return new ApiClient().fetchWithConfig('/jobs/search', "POST", {
				ssn
			});
	}

	getInvoices(jobId) {
			return new ApiClient().fetchWithConfig(`/jobs/${jobId}/invoices`, 'GET');
	}
}

export default JobsData
