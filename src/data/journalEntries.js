import ApiClient from './apiClient';

class JournalEntriesData {
    updateStatus(journalId, statusId) {
        return new ApiClient().fetchWithConfig(`/journal-entries/${journalId}/status`, "PUT", { statusId: statusId });
    }
}

export default JournalEntriesData