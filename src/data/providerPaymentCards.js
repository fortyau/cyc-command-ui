import ApiClient from './apiClient';

class ProviderPaymentCardsData {
    getProviderPaymentCard(id) {
        return new ApiClient().fetchWithConfig(`/provider-payment-cards/${id}`, "GET");
    }

    getProviderPaymentCardsForClaim(claimId) {
        return new ApiClient().fetchWithConfig(`/claims/${claimId}/provider-payment-cards`, "GET")
    }
}

export default ProviderPaymentCardsData