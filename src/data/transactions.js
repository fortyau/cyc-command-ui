import ApiClient from './apiClient';

class TransactionsData {
    getTransaction(id) {
        return new ApiClient().fetchWithConfig(`/transactions/${id}`, "GET");
    }
}

export default TransactionsData