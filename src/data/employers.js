import ApiClient from './apiClient';

class Employers {
    getDetails(id) {
        return new ApiClient().fetchWithConfig(`/employers/${id}`, "GET");
    }

    getEmployers(searchType, searchText) {
      return new ApiClient().fetchWithConfig('/employers/search', 'POST', {
        searchType,
        searchText
      });
    }

    getSingleFile(employerId, fileId) {
        return new ApiClient().fetchWithConfig(`/employers/${employerId}/files/${fileId}`, "GET");
    }

    getFiles(employerId, opts) {
        let params = `pageSize=${opts.pageSize}&pageNumber=${opts.pageNumber}`;
        params = `${params}&cl=${opts.claim ? 1: 0}`;
        params = `${params}&cn=${opts.contribution ? 1: 0}`;
        params = `${params}&ce=${opts.census ? 1: 0}`;
        params = `${params}&en=${opts.enrollment ? 1: 0}`;

        if(opts.dateFrom) {
          params = `${params}&dateFrom=${opts.dateFrom.format("YYYY/MM/DD")}`;
        }

        if(opts.dateTo) {
          params = `${params}&dateTo=${opts.dateTo.format("YYYY/MM/DD")}`;
        }

        return new ApiClient().fetchWithConfig(`/employers/${employerId}/files?${params}`, "GET");
    }
}

export default Employers
