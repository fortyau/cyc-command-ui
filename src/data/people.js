import ApiClient from './apiClient';

class PeopleData {
	getDependentsForClaim(claimId) {
		return new ApiClient().fetchWithConfig(`/people?dependentsForClaim=${claimId}`, "GET")
	}
}

export default PeopleData