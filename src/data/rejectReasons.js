import ApiClient from './apiClient';

class RejectReasonsData {
	getAll() {
		return new ApiClient().fetchWithConfig(`/reject-reasons`, "GET")
	}
}

export default RejectReasonsData