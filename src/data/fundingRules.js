import ApiClient from './apiClient';

class FundingRuleData {
	getTransactionsForFundingRule(id) {
		return new ApiClient().fetchWithConfig(`/fundingRule/${id}`, "GET")
	}
}

export default FundingRuleData