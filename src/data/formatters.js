class Formatters {
    static lastCommaFirst(last, first) {
        return `${last}, ${first}`;
    }

    static currency(num) {
        const formatter = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'});
        return num === undefined || num === null ? num : formatter.format(num);
    }

    static ssnLast4(last4) {
        return `###-##-${last4}`;
    }

    static person(person) {
        return `${person.lastName}, ${person.firstName}`;
    }
}

export default Formatters;