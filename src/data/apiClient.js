import 'isomorphic-fetch'
import SessionStore from 'stores/SessionStore'
import {snackbarActions} from 'stores/SnackbarStore'
import Security from 'data/security'

const login_error = 'User Login required.';

class ApiClient {

    getHeaders(token) {
        return {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        }
    }

    assertToken () {
        return new Promise((resolve, reject) => {
            if (SessionStore.state.expires > new Date().getTime() / 1000) {
                resolve(SessionStore.state.authToken)
            } else if (SessionStore.state.refreshExpires > new Date().getTime() / 1000) {
                new Security().refresh()
                    .then(token => resolve(token))
                    .catch(() => {
                        return reject(login_error);
                    }) // assume refresh token is revoked
            } else {
                new Security().logout();
                reject(login_error);
            }
        })
    }

    parseResponse (response) {
        if (response.status >= 200 && response.status < 300) {
            if (response.status !== 204) return response.json()
        } else if (response.status === 401) {
            throw response
        } else {
            throw response
        }
    }

    getBaseUrl() {
        return process.env.REACT_APP_CMDSVCBASEURL;
    }

    fetchWithConfig(relativeUrl, httpMethod, body) {
        return this.assertToken().then(token => {
            const config = {
                headers: this.getHeaders(token),
                mode: 'cors',
                method: httpMethod,
            };

            if (body)
                config.body = JSON.stringify(body);

            return fetch(`${this.getBaseUrl()}${relativeUrl}`, config);
        })
        .then(response => {
            if (!response.ok) {
            	throw new Error("API Failed");
            }
            if (response.status === 204) {
            	return {};
            }

            return response.json();
        })
        .catch(err => {
            if (err === login_error) {
                // GULP: swallowed. Mmmm yummy.
                // Application header should detect and redirect us.
            }
            else {
                console.log(err);
                snackbarActions.showSnackbar('OOPS Something is wrong', 'error')
            }
            return Promise.reject(err);
        })
    }
}

export default ApiClient;
