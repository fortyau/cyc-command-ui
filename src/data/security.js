import AuthData from 'data/auth'
import jwtDecode from 'jwt-decode'
import { Cookies } from 'react-cookie'
import {sessionActions} from 'stores/SessionStore'
import sessionStore from 'stores/SessionStore'

// todo: move this out of data

class Security {
    constructor() {
        this.cookie = new Cookies();
    }

	isAuthenticated() {
        // todo: test for refresh expired?
        return !!sessionStore.state.refreshToken;
    }

    hasRole(role) {
        return sessionStore.state.roles && sessionStore.state.roles.includes(role);
    }

    hasAtLeastOneRole(requiredRoles) {
        return sessionStore.state.roles &&
            sessionStore.state.roles
                .map(r => r.toLowerCase())
                .some(r => requiredRoles.includes(r));
    }

    hasEveryRole(requiredRoles) {
        return sessionStore.state.roles &&
            requiredRoles
                .every(r => sessionStore.state.roles
                    .some(v => v.toLowerCase() === r.toLowerCase()))
    }

    isExpired() {
        if (!sessionStore.state.refreshToken) {
            return false;
        }

        const decodedJWT = jwtDecode(sessionStore.state.refreshToken);
        return decodedJWT.exp <= new Date().getTime() / 1000;
    }

    parseCookie() {
        const accessToken = this.cookie.get('jwt_access_token');
        const refreshToken = this.cookie.get('jwt_refresh_token');

        if (!accessToken || !refreshToken) {
            return;
        }

        this.updateStoreTokens(accessToken, refreshToken);
    }

    login(user, pass) {
        const cookie = this.cookie;

        return new AuthData()
            .login(user, pass)
            .then(response => {
                if (response.status === 200) {
                    return response.json();
                }
                if (response.status === 403) {
                    throw new Error('The username and password you entered did not match any accounts in our system. Please try again.');
                }
                throw new Error('Something is wrong');
            })
            .then(json => {
                this.updateStoreTokens(json.access_token, json.refresh_token);

                const options = this.getOptions();
                cookie.set('jwt_access_token', json.access_token, options);
                cookie.set('jwt_refresh_token', json.refresh_token, options);

                if (!this.isAuthenticated()) {
                    console.log('Cookie not visible after login. Misconfiguration is likely.');
                }
            })
    }

    refresh() {
        return new AuthData().refresh(sessionStore.state.refreshToken)
            .then(response => {
                if (response.status !== 200) {
                    this.logout();
                    throw new Error ('Refresh rejected');
                }
                return response.json();
            })
            .then(data => {
                this.updateStoreAccessToken(data.access_token);

                const options = this.getOptions();
                this.cookie.set('jwt_access_token', data.access_token, options);

                return data.access_token;
            })
    }

    logout() {
        /* Important! Clear the cookie before executing sessionActions to avoid having the app re-read the cookie immediately. */

        const options = this.getOptions();

        this.cookie.remove('jwt_access_token', options);
        this.cookie.remove('jwt_refresh_token', options);

        sessionActions.setRefreshToken(undefined)
        sessionActions.setAuthToken(undefined)
        sessionActions.setExpires(undefined)
        sessionActions.setRefreshExpires(undefined)
        sessionActions.setRoles(undefined)
    }

    getOptions() {
        let options = {
            path: '/'
        }
        const configSetting = process.env.REACT_APP_COOKIE_DOMAIN;
        if (configSetting) {
            options.domain = configSetting;
        }
        return options;
    }

    updateStoreTokens(accessToken, refreshToken) {
       this.updateStoreAccessToken(accessToken);
       this.updateStoreRefreshToken(refreshToken);
    }

    updateStoreAccessToken(accessToken) {
        sessionActions.setAuthToken(accessToken)
        const decodedAccess = jwtDecode(accessToken)
        sessionActions.setExpires(decodedAccess.exp)
        sessionActions.setRoles(decodedAccess.roles)
    }

    updateStoreRefreshToken(refreshToken) {
        sessionActions.setRefreshToken(refreshToken)
        const decodedRefresh = jwtDecode(refreshToken)
        sessionActions.setRefreshExpires(decodedRefresh.exp)
    }
}

export default Security