import React from 'react'
import Card from 'components/Card'
import PageTitle from 'components/PageTitle'
import SectionTitle from 'components/SectionTitle'
import EmployerData from 'data/employers'
import DrilldownTable from 'components/DrilldownTable'

class EmployerDetail extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        loaded: false,
        results: null
      }
    }

    componentWillMount() {
      new EmployerData()
        .getDetails(this.props.match.params.id)
        .then(data => {
            this.setState({
              loaded: true,
              results: data});
        });
    }

    getColumns() {
      return [
          {
              header: 'Active',
              getText: (rawObj) => rawObj.state === 'Active' ? 'Y' : 'N'
          },
          {
              header: 'Employer ID:Name',
              getText: (rawObj) =>  `${rawObj.employer.id}:${rawObj.employer.name}`
          },
          {
              header: 'Tax ID',
              getText: (rawObj) => rawObj.employer.taxId,
          },
          {
              header: 'Distributor',
              getText: (rawObj) => rawObj.distributor.name
          },
          {
              header: 'Sponsor',
              getText: (rawObj) => rawObj.sponsor.name
          },
          {
              header: 'Broker Firm',
              getText: (rawObj) => rawObj.employer.broker.firm
          },
          {
              header: 'Sponsor Bank',
              getText: (rawObj) => rawObj.sponsorBank.name
          },
          {
              header: 'Enrollment Partner',
              getText: (rawObj) => rawObj.enrollmentPartner
          },
          {
               header: 'Account Manager',
               getText: (rawObj) => {
                 let firstName = rawObj.accountManager.firstName || '';
                 let middleName = rawObj.accountManager.middleName || '';
                 let lastName = rawObj.accountManager.lastName ?
                   `${rawObj.accountManager.lastName},` : '';
                 return `${lastName} ${firstName} ${middleName}`.trim();
               }
            }
      ];
    }

    rowHighlightCondition(obj) {
      return obj.state !== 'Active';
    }

    render () {
        return (
          <Card>
              <PageTitle value="Employer Detail"/>

              {
                  this.state.results ?
                      <div className="margin-top">
                          <SectionTitle value="Employer" />
                          <DrilldownTable
                              rowHighlightColor="#f6f0ee"
                              rowHighlightCondition={this.rowHighlightCondition}
                              cols={this.getColumns()}
                              source={[this.state.results]} />
                      </div>
                  : this.state.loaded ?
                    <div className="margin-top">Employer not found</div> : null
                }

          </Card>
        )
    }
}

export default EmployerDetail
