import React from 'react';
import PageTitle from "components/PageTitle";
import DrilldownTable from 'components/DrilldownTable'
import Loading from 'components/Loading'
import Card from 'components/Card'
import ClaimsData from "data/claims";
import ProviderPaymentCardData from 'data/providerPaymentCards'
import SectionTitle from "../../components/SectionTitle";
import TextField from "../../components/TextField";
import Formatters from "../../data/formatters";

import './ProviderPaymentCards.scss'

const cardStatusLookup = {
    2962: "Deleted",
    2963: "Activated",
    130003: "Recycled",
    2700: "Unsent",
    20156: "Voided",
    2960: "To be deleted"
};

const claimCols = [
    {
        header: 'Charge Id',
        property: 'claimId',
        isLink: true
    },
    {
        header: 'Claim Number',
        property: 'submissionId'
    }, {
        header: 'Claim Type',
        property: 'claimType',
    }
];

class ProviderPaymentCards extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            providerPaymentCardId: props.match.params.id,
            providerPaymentCard: {},
            loading: true,
            relatedClaims: []
        };
    }

    componentDidMount() {
        this.setState({loading: true});

        const claimData = new ClaimsData();
        const providerPaymentCardData = new ProviderPaymentCardData();

        const getClaims = claimData.getClaimsRelatedToProviderPaymentCard(this.state.providerPaymentCardId)
            .then(data => this.setState({relatedClaims: data}));

        const getProviderPaymentCard = providerPaymentCardData.getProviderPaymentCard(this.state.providerPaymentCardId)
            .then(data => this.setState({providerPaymentCard: data}));

        Promise.all([getClaims, getProviderPaymentCard])
            .then(data => this.setState({loading: false}))
    }

    buildLink = claim => '/claims/' + claim.claimId;

    renderClaims() {
        return (
            <div>
                <div>This payment included the following claims:</div>
                <DrilldownTable
                    cols={claimCols}
                    source={this.state.relatedClaims}
                    buildLink={this.buildLink}
                    paginationEnabled={true}
                    pageSize={10}/>
            </div>
        )
    }

    renderPayments() {

        const {
            cardNumber,
            cardAmount,
            amtProcessed,
            cardStatus,
            issuedDate
        } = this.state.providerPaymentCard;

        return (
            <div>
                <SectionTitle value={"Payment Card Details"}/>
                <div className="payment-detail-row">
                    <TextField label="Card Number" value={cardNumber}/>
                    <TextField label="Issued Date" value={issuedDate}/>
                    <TextField label="Card Amount" value={cardAmount ? cardAmount : 0} formatter={Formatters.currency}/>
                    <TextField label="Amt Processed" value={amtProcessed ? amtProcessed : 0}
                               formatter={Formatters.currency}/>
                    <TextField label="Card Status" value={cardStatusLookup[cardStatus]}/>
                </div>
            </div>
        )
    }

    render() {

        return (
            <Card>

                {this.state.loading ?
                    <Loading/> :
                    <div>
                        <PageTitle value={`Payment Card ${this.state.providerPaymentCard.cardNumber}`}/>
                        <div className="margin-top">
                            {this.renderClaims()}
                            {this.renderPayments()}
                        </div>
                    </div>}
            </Card>
        )
    }

}

export default ProviderPaymentCards;