import React from 'react'
import Card from 'components/Card'
import PageTitle from 'components/PageTitle'
import SearchBox from 'components/SearchBox'
import {snackbarActions} from 'stores/SnackbarStore'
import EmployerData from 'data/employers'
import EmployerSearchResults from './EmployerSearchResults'
import Loading from 'components/Loading'

class FileMonitor extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        results: [],
        searching: false,
        searchCompleted: false,
        searches: this.getSearches()
      };
    }

    getSearches() {
      return [
        {
          searchType: 'Employer',
          searchText: '',
          placeholder: 'Employer ID or Name',
          errorMessages: {
            missingValue: 'Employer ID or name required'
          }
        },
        {
          searchType: 'File',
          searchText: '',
          placeholder: 'File Name',
          errorMessages: {
            missingValue: 'File name required'
          }
        }
      ];
    }

    handleSearchChange(searchType, value) {
      this.updateSearch(searchType, 'searchText', value.trim());
    }

    updateSearch(searchType, property, value) {
      const searches = this.state.searches;

      searches.forEach(s => {
        if(s.searchType === searchType) {
          s[property] = value;
        }
      });

      this.setState({
        searches
      });
    }

    getSearch(searchType) {
      return this.state.searches.filter(s => s.searchType === searchType)[0];
    }

    handleSearchSubmit(searchType) {
      const search = this.getSearch(searchType);

      if(search.searchText) {
        this.getEmployers(searchType, search.searchText);
      } else {
        snackbarActions.showSnackbar(search.errorMessages.missingValue, "info");
      }
    }

    getEmployers(searchType, searchText) {
      this.setState({results: [], searching: true});
      new EmployerData()
          .getEmployers(searchType, searchText)
          .then(data => {
            this.setState({
              searching: false,
              results: data});

              if(data.length === 0) {
                snackbarActions.showSnackbar("No employers found", "info");
              } else if(searchType === "Employer" && data.length === 1) {
                this.redirectToDetails(data[0].employer.id);
              } else if(searchType === "File") {
                if(data.length === 1) {
                  this.redirectToDetails(data[0].employer.id, data[0].file.id);
                } else {
                  snackbarActions.showSnackbar("More than one file found", "info");
                }
              }
          });
    }

    redirectToDetails(employerId, fileId) {
      let params = "";

      if(fileId) {
        params = `?fid=${fileId}`;
      }

      this.props.history.push(`/filemonitor/employers/${employerId}${params}`);
    }

    renderSearchBoxes() {
      return this.state.searches.map(s => (
          <div className="search-input" key={s.searchType}>
            <SearchBox
              label=""
              placeholder={s.placeholder}
              onChange={(value) => this.handleSearchChange(s.searchType, value)}
              onSubmit={() => this.handleSearchSubmit(s.searchType)}/>
          </div>));
    }

    renderEmployerSearchResults() {
      let results = null;

      if(!this.state.searching && this.state.results.length > 0) {
        results = (
          <EmployerSearchResults source={this.state.results} />
        );
      }

      return results;
    }

    render () {
        return (
          <Card>
              <PageTitle value="File Monitor" className="form-formatting"/>
                  {this.renderSearchBoxes()}
                  {this.renderEmployerSearchResults()}
                  {this.state.searching ? <Loading /> : null}
          </Card>
        )
    }
}

export default FileMonitor
