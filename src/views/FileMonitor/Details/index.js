import React from 'react'
import Card from 'components/Card'
import PageTitle from 'components/PageTitle'
import EmployerDetails from './EmployerDetails'
import FileDetails from './FileDetails'
import Button from 'components/Button'
import querystring from 'querystring'

class Details extends React.Component {
    handleNewSearchClick() {
      this.props.history.push("/filemonitor");
    }

    getFileId() {
      let fileId;

      if(this.props.location.search) {
        const qs = querystring.parse(this.props.location.search.split("?")[1]);

        if(qs.fid) {
          fileId = qs.fid;
        }
      }

      return fileId
    }

    render () {
        return (
          <Card>
            <PageTitle value="File Monitor"/>
            <Button className='button button-primary margin-top'
                    events={{onClick: this.handleNewSearchClick.bind(this)}}>
                    New File Search
            </Button>
            <EmployerDetails id={this.props.match.params.employerId} />
            <FileDetails employerId={this.props.match.params.employerId}
                         fileId={this.getFileId()}
                         history={this.props.history} />
          </Card>
        )
    }
}

export default Details
