import React from 'react'
import SectionTitle from 'components/SectionTitle'
import MultiPageTable from 'components/MultiPageTable'
import Loading from 'components/Loading'
import EmployerData from 'data/employers'
import utils from 'lib/utils'
import {mapDateToFormWithTime} from 'lib/date'
import DateRange from 'components/DateRange'
import {snackbarActions} from 'stores/SnackbarStore'
import Button from 'components/Button'
import TextField from 'components/TextField'

import '../FileMonitor.scss'

class FileDetails extends React.Component {
    constructor(props) {
      super(props);
      this.state = this.getDefaults();
    }

    getDefaults() {
      return {
        fileDetailRendered: null,
        dateFrom: null,
        dateTo: null,
        searching: false,
        pageCount: 0,
        pageSize: 0,
        pageNumber: 0,
        contribution: true,
        enrollment: true,
        census: true,
        claim: true
      };
    }

    getColumns() {
      return [
          {
            header: 'File Name',
            getText: (rawObj) => rawObj.fileName,
            clickable: true,
            onClick: (rawObj) => this.renderFileDetail(rawObj)
          },
          {
            header: 'Received Date',
            getText: (rawObj) => mapDateToFormWithTime(rawObj.receivedDate)
          },
          {
            header: 'Processed Date',
            getText: (rawObj) => mapDateToFormWithTime(rawObj.processedDate)
          },
          {
            header: 'File Size',
            getText: (rawObj) => utils.Bytes.toSizeString(rawObj.fileSize)
          },
          {
            header: 'File Type',
            getText: (rawObj) => rawObj.type
          },
          {
            header: 'Status',
            getText: (rawObj) => rawObj.status
          },
          {
            header: 'Received',
            getText: (rawObj) => rawObj.received.toLocaleString()
          },
          {
            header: 'Processed',
            getText: (rawObj) => rawObj.processed.toLocaleString()
          },
          {
            header: 'Rejected',
            getText: (rawObj) => rawObj.rejected.toLocaleString()
          }
      ];
    }

    getSingleFile(employerId, fileId) {
      return new EmployerData()
        .getSingleFile(employerId, fileId)
        .then(fileInfo => {
          return {
            pageInfo: {
              pageSize: this.refs.files.pageSize,
              currentPage: 1,
              totalItems: 1,
              pages: fileInfo  ? 1 : 0
            },
            items: [fileInfo]
          }
        });
    }

    getAllFiles(employerId, opts) {
      return new EmployerData().getFiles(employerId, opts);
    }

    handlePageRequest(pageSize, pageNumber, preFetch) {
      const { dateFrom, dateTo } = this.state;

      this.setState({
        searching: true,
        pageSize,
        pageNumber
      });

      if(this.props.fileId && preFetch) {
        return this.getSingleFile(this.props.employerId, this.props.fileId);
      } else {
        const opts = {
            pageSize,
            pageNumber,
            dateFrom,
            dateTo,
            contribution: this.state.contribution,
            census: this.state.census,
            claim: this.state.claim,
            enrollment: this.state.enrollment
        };

        return this.getAllFiles(this.props.employerId, opts);
      }
    }

    handlePageRequestComplete(pageCount, items) {
      this.setState({
        searching: false,
        pageCount
      });

      if(items.length === 0) {
        snackbarActions.showSnackbar("No files found", "info");
      } else if(items.length === 1) {
        this.renderFileDetail(items[0]);
      }
    }

    handleCheckboxChange(event) {
      const type = event.target.name;
      const checked = event.target.checked;

      this.setState({
        [type]: checked
      });
    }

    handleDateFromChange(dateFrom) {
      this.setState({
        dateFrom
      });
    }

    handleDateToChange(dateTo) {
      this.setState({
        dateTo
      });
    }

    handleSearchClick() {
      const { dateFrom, dateTo } = this.state;

      if(dateFrom && dateTo && dateFrom.isAfter(dateTo)) {
        snackbarActions.showSnackbar('Start date must be on or before end date', 'info');
      } else {
        this.refs.files.search();
      }
    }

    handleFilterResetClick() {
      this.refs.dateRange.reset();
      this.setState(this.getDefaults());
    }

    renderCheckbox(name) {
      return (
        <div>
          {name}
          <input className={'soft-text'}
                 name={name.toLowerCase()}
                 type="checkbox"
                 checked={this.state[name.toLowerCase()]}
                 onChange={this.handleCheckboxChange.bind(this)}/>
        </div>
      );
    }

    renderFileDetail(rawObj) {
      const fileDetailRendered = (
          <div className="file-details">
            {this.getColumns().map((c, i) =>
                <TextField key={i} label={c.header} value={c.getText(rawObj)}/>
            )}
          </div>
      );

      this.setState({
        fileDetailRendered
      });
    }

    renderFilterControls() {
      return (
          <div>
            <DateRange label = "Date Range"
                       ref = "dateRange"
                       dateFromLabel = "Date From"
                       dateToLabel = "Date To"
                       onDateFromChange={this.handleDateFromChange.bind(this)}
                       onDateToChange={this.handleDateToChange.bind(this)}>
                <Button className='button button-primary apply-button'
                        events={{onClick: this.handleSearchClick.bind(this)}}>
                        Search
                </Button>
                <Button className='button button-primary apply-button'
                        events={{onClick: this.handleFilterResetClick.bind(this)}}>
                        Reset Filters
                </Button>
            </DateRange>
            <div className={'check-boxes margin-top'}>
              {this.renderCheckbox("Contribution")}
              {this.renderCheckbox("Census")}
              {this.renderCheckbox("Enrollment")}
              {this.renderCheckbox("Claim")}
            </div>
        </div>
      );
    }

    render () {
        return (
          <div>
            <SectionTitle value="File Details" />
            { this.state.fileDetailRendered ||
              <div>Select a file below to view details</div>}
            <SectionTitle value="All Files" />
            { this.renderFilterControls() }
            <br />
            <MultiPageTable
              ref="files"
              pageSize={10}
              enableApplyButton={false}
              onPageRequest={this.handlePageRequest.bind(this)}
              onPageRequestComplete={this.handlePageRequestComplete.bind(this)}
              cols={this.getColumns()}/>
              { this.state.searching ? <Loading /> : null }
          </div>
        )
    }
}

export default FileDetails
