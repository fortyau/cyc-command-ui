import React from 'react'
import SectionTitle from 'components/SectionTitle'
import DrilldownTable from 'components/DrilldownTable'
import Loading from 'components/Loading'
import EmployerData from 'data/employers'

class EmployerDetails extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        employer: {
          loaded: false,
          details: []
        }
      };
    }

    componentWillMount() {
      new EmployerData()
          .getDetails(this.props.id)
          .then(data => {
              this.setState({
                employer: {
                  loaded: true,
                  details: [data]
                }});
          });
    }

    getColumns() {
      return [
          {
              header: 'Employer',
              getText: (rawObj) => rawObj.employer.name
          },
          {
              header: 'Distributor',
              getText: (rawObj) => rawObj.distributor.name
          },
          {
              header: 'Sponsor',
              getText: (rawObj) => rawObj.sponsor.name
          },
          {
              header: 'Enrollment Partner ID',
              getText: (rawObj) => rawObj.enrollmentPartnerId || 'NO_PARTNER'
          },
          {
              header: 'Enrollment Submitter ID',
              getText: (rawObj) => rawObj.enrolmentSubmitterId || "NO_SUBMITTER"
          }
      ];
    }

    render () {
        return (
          <div className="margin-top">
            <SectionTitle value="Employer Details" />
            { this.state.employer.loaded ?
              <DrilldownTable
                cols={this.getColumns()}
                source={this.state.employer.details} /> : <Loading/> }
          </div>
        )
    }
}

export default EmployerDetails
