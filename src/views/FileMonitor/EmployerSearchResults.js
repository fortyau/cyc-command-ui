import React from 'react'
import DrilldownTable from 'components/DrilldownTable'
import { withRouter } from 'react-router'

class EmployerSearchResults extends React.Component {
    getColumns() {
      return [
          {
              header: '',
              clickable: true,
              onClick: (rawObj) => this.props.history.push(`/filemonitor/employers/${rawObj.employer.id}`),
              getText: (rawObj) => 'Select'
          },
          {
              header: 'Employer',
              getText: (rawObj) => rawObj.employer.name
          },
          {
              header: 'Distributor',
              getText: (rawObj) => rawObj.distributor.name
          },
          {
              header: 'Sponsor',
              getText: (rawObj) => rawObj.sponsor.name
          }
      ];
    }

    render () {
        return (
          <DrilldownTable
            pageSize={10}
            cols={this.getColumns()}
            source={this.props.source} />
        )
    }
}

export default withRouter(EmployerSearchResults)
