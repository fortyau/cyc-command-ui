import React from 'react'
import Card from 'components/Card'
import PageTitle from 'components/PageTitle'
import DrilldownTable from 'components/DrilldownTable'
import Modal from 'components/Modal'
import TransactionsData from 'data/transactions';
import AccountsData from 'data/accounts';
import JournalEntriesData from 'data/journalEntries';
import Loading from 'components/Loading'
import TextField from 'components/TextField'
import Formatters from 'data/formatters'
import {snackbarActions} from 'stores/SnackbarStore'
import ButtonBar from 'components/ButtonBar'
import RecentClaimStore from "../../stores/RecentClaimStore";
import Security from 'data/security'

class AccountDetails extends React.Component {
    constructor(props) {
        super(props);
        this.stores = [RecentClaimStore];
        this.state = {
            accountId: props.match.params.id,
            account: {},
            loading: true,
            transactionLoading: false,
            modal: false,
            detailId: null,
            detailDone: false
        };
        this.onClick = this.onClick.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.onModalClose = this.onModalClose.bind(this);
    }

    componentDidMount() {
        this.setState({loading: true});
        const accountData = new AccountsData();

        accountData.getAccount(this.state.accountId)
            .then(data => this.setState({account: data, loading: false}))
    }

    onClick(transaction) {
        this.setState({modal: true, transactionLoading: true, detailId: transaction.id});

        const data = new TransactionsData();
        data.getTransaction(transaction.id)
            .then(response => {
                this.setState({detail: response, transactionLoading: false});
            })
    }

    onModalClose() {
        this.setState({modal: false, detailDone: false});
    }


    handleClick(action) {
        if (action === 'Mark as Credit') {
            const data = new JournalEntriesData();
            data.updateStatus(this.state.detail.journalId, 12001 /* Rollover */)
                .then(response => {
                    if (response) {
                        this.setState({detailDone: true});
                        snackbarActions.showSnackbar('Marked as Credit', 'info')
                    }
                });
        }
    }

    renderAccountDetails() {

        const {
            name,
            description,
            accountTypeName,
            offeringName,
            rolloverBalance,
            balance,
            status,
            gracePeriod,
            fdrBalance,
            timelyFilingDays
        } = this.state.account;

        return (
            <div className="grid-1">
                <TextField label="Account Name" value={name}/>
                <TextField label="Account Description" value={description}/>
                <TextField label="Account Type" value={accountTypeName}/>
                <TextField label="Offering Name" value={offeringName}/>
                {timelyFilingDays && timelyFilingDays > 0 ? <TextField label="Rollover Balance" value={rolloverBalance} formatter={Formatters.currency}/> : null}
                <TextField label="Available Balance" value={balance} formatter={Formatters.currency}/>
                <TextField label="Account Status" value={status}/>
                <TextField label="Grace Period" value={gracePeriod ? "Yes" : "No"}/>
                {fdrBalance[1] ? <TextField label="FDR Balance" formatter={Formatters.currency} value={fdrBalance[1]}/> : <TextField label="FDR Balance" value='N/A'/>}
            </div>
        )
    }

    renderModal() {

        const sec = new Security();
        const showMarkAsCredit = () =>
            sec.hasEveryRole(['CYCCEdit', 'claimssupervisor']) &&
            !this.state.detailDone &&
            this.state.detail.type !== 'Debit';

        return (
            <Modal
                header="Transaction Details"
                onClose={this.onModalClose.bind(this)}
                showClose={true}>
                {this.state.transactionLoading ?
                    <Loading/>
                    : <div>
                        <div className="grid-2">
                            <TextField label="Date" value={this.state.detail.date}/>
                            <TextField label="Journal Id" value={this.state.detail.journalId}/>
                        </div>
                        <div className="grid-2">
                            <TextField label="Account" value={this.state.detail.accountName}/>
                            <TextField label="Status" value={this.state.detail.status}/>
                        </div>
                        <div className="grid-2">
                            <TextField label="Memo" value={this.state.detail.memo}/>
                            <TextField label="Amount" value={this.state.detail.amount} formatter={Formatters.currency}/>
                        </div>
                        <div className="grid-2">
                            <TextField label="Type" value={this.state.detail.type}/>
                            <TextField label="Invoice Id" value={this.state.detail.invoiceId}/>
                        </div>
                        {showMarkAsCredit() && <ButtonBar actions={['Mark as Credit']} onClick={this.handleClick}/>}
                    </div>}
            </Modal>)
    }


    formatPostings() {
        const postings = this.state.account.postings.map(posting => ({
                memo: posting.memo,
                id: posting.id,
                journalType: posting.journalType,
                settledTransactionDate: posting.settledTransactionDate,
                amount: Formatters.currency(posting.amount)
            })
        );

        return postings.sort(function (a, b) {
            let keyA = new Date(a.settledTransactionDate),
                keyB = new Date(b.settledTransactionDate);

            // Compare the 2 dates
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        }).reverse();
    }

    render() {
        const transactionCols = [
            {
                header: 'Activity',
                property: 'memo',
                clickable: true,
                onClick: this.onClick
            },
            {
                header: 'Type',
                property: 'journalType',
                clickable: false
            },
            {
                header: 'Date',
                property: 'settledTransactionDate',
                clickable: false
            },
            {
                header: 'Amount',
                property: 'amount',
                clickable: false
            }
        ];

        return (
            <Card>
                <ButtonBar actions={['Return To Claim']} onClick={() => window.location = `/claims/${RecentClaimStore.state.claimId}`}/>
                <PageTitle value={'Account # ' + this.state.accountId}/>
                {this.state.loading ?
                    <Loading/> :
                    <div>
                        {this.renderAccountDetails()}
                        <DrilldownTable
                            cols={transactionCols}
                            source={this.formatPostings()}/>
                    </div>}
                {this.state.modal && this.renderModal()}
            </Card>
        )
    }
}

export default AccountDetails