import React from 'react'
import Modal from 'components/Modal'
import TextField from 'components/TextField'
import Loading from 'components/Loading'
import Security from 'data/security'

class LoginForm extends React.Component {
    constructor (props) {
        super (props);
        this.state = {
            username: '',
            password: '',
            error: '',
            loading: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
      }

      componentDidMount() {
          const sec = new Security();
          sec.parseCookie();
          if (sec.isAuthenticated() && !sec.isExpired()) {
            this.props.history.push(this.getFutureRoute());
          }
      }

      handleSubmit(event) {
        this.setState({loading: true});

        const sec = new Security();
        sec.login(this.state.username, this.state.password)
            .then(() => {
                if (!sec.hasAtLeastOneRole(['cyccedit', 'cyccview'])) {
                    throw new Error ('User does not have access to CYC Command');
                }

                this.props.history.push(this.getFutureRoute());
            })
            .catch(error => {
                this.setState({loading: false, error: error.message});
            })

        event.preventDefault();
      }

      getFutureRoute() {
        let to = '/';
        if (this.props.history.location.state && this.props.history.location.state.from && this.props.history.location.state.from.pathname) {
            to = this.props.history.location.state.from.pathname;
        }
        return to;
      }

      handleChange(key, val) {
        const newState = {};
        newState[key] = val;
        this.setState(newState);
      }

      render () {
        return (
            <div>
                <Modal header="Login">
                    {this.state.loading ? 
                        <Loading />
                        : <form onSubmit={this.handleSubmit}>
                            <TextField autoFocus={true} label="Username" value={this.state.username} editable={true} size={40} onChange={(event => this.handleChange('username', event))} required={true} />
                            <TextField label="Password" value={this.state.password} editable={true} size={40} onChange={(event => this.handleChange('password', event))} required={true} type='password' />
                            <div className="margin-top">
                                <button className="button button-primary" type="submit" onClick={this.handleSubmit}>
                                    Submit
                                </button>
                                
                            </div>
                            {this.state.error ? <div className='margin-top error-text'>{this.state.error}</div> : null}
                        </form>}
                </Modal>
            </div>
        )
      }
}

export default LoginForm