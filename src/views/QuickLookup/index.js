import React from 'react'
import Card from 'components/Card'
import PageTitle from 'components/PageTitle'
import SearchBox from 'components/SearchBox'
import valid from 'lib/valid'
import {quickLookupStoreActions} from 'stores/QuickLookupStore'
import {snackbarActions} from 'stores/SnackbarStore'

class QuickLookup extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        searches: this.getSearches()
      };
    }

    getSearches() {
      return [
        {
          searchType: 'CONTRIBUTIONS',
          searchText: '',
          label: 'Search for Contributions',
          placeholder: 'Employee SSN',
          url: '/quicklookup/contributions',
          validation: {
            isValid: (value) => { return valid.ssn(value); },
            errorMessages: {
              invalidValue: 'Invalid SSN',
              missingValue: 'SSN is required'
            }
          }
        },
        {
          searchType: 'EMPLOYER',
          searchText: '',
          label: 'Search for Employers',
          placeholder: 'Employer ID or Name',
          url: '/quicklookup/employers',
          error: '',
          validation: {
            isValid: () => { return true; },
            errorMessages: {
              invalidValue: '',
              missingValue: 'Employer ID or name required'
            }
          }
        }
      ];
    }

    handleSearchChange(searchType, value) {
      this.updateSearch(searchType, 'searchText', value);
    }

    updateSearch(searchType, property, value) {
      const searches = this.state.searches;

      searches.forEach(s => {
        if(searchType === 'ALL' || s.searchType === searchType) {
          s[property] = value;
        }
      });

      this.setState({
        searches
      });
    }

    getSearch(searchType) {
      return this.state.searches.filter(s => s.searchType === searchType)[0];
    }

    handleSearchSubmit(searchType) {
      this.updateSearch('ALL', 'error', '');
      const search = this.getSearch(searchType);

      if(search.searchText) {
        if(search.validation.isValid(search.searchText)) {
          quickLookupStoreActions.setSearch(searchType, search.searchText);
          this.props.history.push(search.url);
        } else {
          snackbarActions.showSnackbar(search.validation.errorMessages.invalidValue, "info");
        }
      } else {
        snackbarActions.showSnackbar(search.validation.errorMessages.missingValue, "info");
      }
    }

    render () {
        return (
          <Card>
              <PageTitle value="Quick Look Up"/>
              <div className="margin-top">
                  {this.state.searches.map(s => (<div key={s.searchType}>
                        <SearchBox
                          label={s.label}
                          placeholder={s.placeholder}
                          onChange={(value) => this.handleSearchChange(s.searchType, value)}
                          onSubmit={() => this.handleSearchSubmit(s.searchType)}/>
                          <br />
                      </div>))}
              </div>
          </Card>
        )
    }
}

export default QuickLookup
