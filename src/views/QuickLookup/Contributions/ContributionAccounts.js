import React from 'react'
import DrilldownTable from 'components/DrilldownTable'
import SectionTitle from 'components/SectionTitle'
import {mapDateToForm} from 'lib/date.js'

class ContributionAccounts extends React.Component {
    getColumns() {
      return [
        {
          header: 'First Name',
          getText: (rawObj) => (rawObj.accountHolder && rawObj.accountHolder.firstName) || '?'
        },
        {
          header: 'Last Name',
          getText: (rawObj) => (rawObj.accountHolder && rawObj.accountHolder.lastName) || '?'
        },
        {
          header: 'Date of Birth',
          getText: (rawObj) => (rawObj.accountHolder && rawObj.accountHolder.dateOfBirth) ? mapDateToForm(rawObj.accountHolder.dateOfBirth) : '?'
        },
        {
          header: 'Bank',
          getText: (rawObj) => rawObj.bankName || '?'
        },
        {
          header: 'Account Type',
          getText: (rawObj) => rawObj.accountType || '?'
        }
      ];
    }

    render () {
        return (
          <div>
            <SectionTitle value="Employee" />
            <DrilldownTable
                cols={this.getColumns()}
                source={this.props.source}/>
          </div>
      )
    }
}

export default ContributionAccounts
