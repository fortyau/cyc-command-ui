import React from 'react'
import Accounts from './ContributionAccounts'
import Jobs from './ContributionJobs'

class ContributionSearchResults extends React.Component {
    render () {
        return (
          <div>
            <Accounts source={this.props.source.bankAccounts} />
            <Jobs source={this.props.source.jobs} />
          </div>
      )
    }
}

export default ContributionSearchResults
