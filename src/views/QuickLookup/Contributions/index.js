import React from 'react'
import Card from 'components/Card'
import PageTitle from 'components/PageTitle'
import SearchBox from 'components/SearchBox'
import JobData from 'data/jobs'
import Loading from 'components/Loading'
import JobSearchResults from './ContributionSearchResults'
import QuickLookupStore from 'stores/QuickLookupStore'
import {quickLookupStoreActions} from 'stores/QuickLookupStore'
import valid from 'lib/valid'
import {snackbarActions} from 'stores/SnackbarStore'

class ContributionSearch extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          searchText: '',
          results: [],
          searching: false,
          searchCompleted: false
      };
    }

    componentWillMount() {
      const searchText = QuickLookupStore.state.search.text;

      if(searchText) {
        quickLookupStoreActions.clearSearch();
        this.handleSearchChange(searchText);
        this.handleSearchSubmit(searchText);
      }
    }

    handleSearchSubmit(searchText) {
      if(searchText) {
        if(valid.ssn(searchText)) {
          this.setState({searching: true});
          new JobData()
              .getJobs(searchText.split('-').join(''))
              .then(data => {
                  this.setState({
                    searching: false,
                    searchClicked: false,
                    searchCompleted: true,
                    results: data});
              });
          } else {
            snackbarActions.showSnackbar("Invalid SSN", "info");
          }
        } else {
          snackbarActions.showSnackbar("SSN is required", "info");
        }
    }

    handleSearchChange(searchText) {
        this.setState({
            searchText
        });
    }

    showResults() {
      var results = null;

      if(this.state.results.jobs && this.state.results.jobs.length > 0) {
        results = (<JobSearchResults source={this.state.results} />);
      } else if(this.state.searchCompleted) {
        snackbarActions.showSnackbar("No contributions found", "info");
      }

      return results;
    }

    render () {
        return (
          <Card>
              <PageTitle value="Find Contributions and Invoices by Employee"/>

              <div className="margin-top">
                  <SearchBox
                      placeholder='Enter Employee SSN'
                      value={this.state.searchText}
                      onChange={this.handleSearchChange.bind(this)}
                      onSubmit={this.handleSearchSubmit.bind(this)}
                  />
                  { this.state.searching ? <Loading /> : this.showResults() }
              </div>
          </Card>
        )
    }
}

export default ContributionSearch
