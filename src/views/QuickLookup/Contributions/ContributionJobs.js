import React from 'react'
import DrilldownTable from 'components/DrilldownTable'
import SectionTitle from 'components/SectionTitle'
import Loading from 'components/Loading'
import Invoices from './ContributionInvoices'
import JobData from 'data/jobs'
import {mapDateToForm} from 'lib/date.js'

class ContributionJobs extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        results: [],
        jobId: null,
        searching: false,
        searched: false,
        jobIdClicked: false
      }
    }

    getColumns() {
      return [
          {
              header: 'Employer ID',
              getText: (rawObj) => rawObj.employer.id
          },
          {
              header: 'Sponsor:Distributor:Group/Employer',
              getText: (rawObj) =>  `${rawObj.sponsor.name}:${rawObj.distributor.name}:${rawObj.employer.name}`
          },
          {
              header: 'Job ID',
              getText: (rawObj) => rawObj.job.id,
              clickable: true,
              onClick: (rawObj) => this.getInvoices(rawObj)
          },
          {
              header: 'Job Begin Date',
              getText: (rawObj) => mapDateToForm(rawObj.job.startDate)
          },
          {
              header: 'Job End Date',
              getText: (rawObj) => mapDateToForm(rawObj.job.endDate)
          },
          {
              header: 'Job Status',
              getText: (rawObj) => rawObj.job.status.toUpperCase()
          }
      ];
    }

    getInvoices(rawObj) {
      var jobId =  rawObj.job.id;

      if(jobId) {
        this.setState({jobIdClicked: true, searching: true});
        new JobData()
            .getInvoices(jobId)
            .then(data => {
                this.setState({
                  jobId,
                  searched: true,
                  searching: false,
                  results: data});
        });
      }
    }

    showResults() {
      var results = null;

      if(this.state.jobIdClicked) {
        results = (<Invoices jobId={this.state.jobId} source={this.state.results} />);
      }

      return results;
    }

    render () {
        return (
          <div>
            <SectionTitle value="Jobs" />

            <DrilldownTable
                cols={this.getColumns()}
                source={this.props.source} />

            { this.state.searching ? <Loading/> : this.showResults() }
          </div>
      )
    }
}

export default ContributionJobs
