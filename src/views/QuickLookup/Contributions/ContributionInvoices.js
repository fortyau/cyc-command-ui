import React from 'react'
import DrilldownTable from 'components/DrilldownTable'
import SectionTitle from 'components/SectionTitle'
import Field from 'components/forms/Field'
import Button from 'components/Button'
import moment from 'moment'
import {mapDateToForm} from 'lib/date.js'
import Formatters from 'data/formatters'
import {snackbarActions} from 'stores/SnackbarStore'

import './ContributionInvoices.scss'

class ContributionInvoices extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        startDate: null,
        endDate: null,
        hasFiltered: false,
        displayInvoices: this.props.source
      };
    }

    getColumns() {
      return [
        {
          header: 'Memo',
          getText: (rawObj) => rawObj.journal.memo
        },
        {
          header: 'Amt',
          getText: (rawObj) => Formatters.currency(rawObj.posting.amount)
        },
        {
          header: 'Invoice ID',
          getText: (rawObj) => rawObj.invoice.id
        },
        {
          header: 'Invoice Status',
          getText: (rawObj) => rawObj.invoice.status
        },
        {
          header: 'Transaction Date',
          getText: (rawObj) => mapDateToForm(rawObj.journal.transactionDate)
        },
        {
          header: 'Bank Sent Date',
          getText: (rawObj) =>  mapDateToForm(rawObj.posting.bankSentDate)
        },
        {
          header: 'Expected Bank Date',
          getText: (rawObj) =>  mapDateToForm(rawObj.posting.expectedBankDate)
        },
        {
          header: 'Settle Status',
          getText: (rawObj) => rawObj.journal.status
        }
      ];
    }

    hasInvoices() {
      return this.getInvoices().length > 0;
    }

    getInvoices() {
      return this.state.displayInvoices || []
    }

    showDateFilter() {
      return this.hasInvoices() || this.state.hasFiltered;
    }

    handleDateStartChange(event) {
      var value = event.target.value;

      if(value) {
        value = moment(value, 'YYYY-MM-DD');
      }

      this.setState({
        startDate: value
      });
    }

    handleDateEndChange(event) {
      var value = event.target.value;

      if(value) {
        value = moment(value, 'YYYY-MM-DD');
      }

      this.setState({
        endDate: value
      });
    }

    handleClick() {
      const { startDate, endDate } = this.state;
      var displayInvoices = this.props.source;

      if(startDate || endDate) {
        if(startDate && endDate && startDate.isAfter(endDate)) {
          snackbarActions.showSnackbar("Start date must be on or before end date", "info");
          displayInvoices = [];
        } else {
          displayInvoices = this.props.source
            .filter(i => !startDate || moment(i.journal.transactionDate, 'YYYY-MM-DD') >= startDate)
            .filter(i => !endDate || moment(i.journal.transactionDate, 'YYYY-MM-DD') <= endDate);
        }
      }

      this.setState({
        hasFiltered: true,
        displayInvoices
      });
    }

    rowHighlightCondition(obj) {
      return obj.journal.status === 'Open'
    }

    render () {
        const title = `Invoices & Contributions (Job ID ${this.props.jobId})`;
        return (
          <div>
              <SectionTitle value={title} />
              {
                  this.showDateFilter() ?
                    <div style={{'display': 'flex', 'flexFlow': 'row wrap'}}>
                        <div className={'range-label'}>Filter by Date Range</div>
                        <Field className={'form-input'} name="startDate" type="date" label="Start Date" events={{onChange: this.handleDateStartChange.bind(this)}}/>
                        <Field className={'form-input'} name="endDate" type="date" label="End Date" events={{onChange: this.handleDateEndChange.bind(this)}}/>
                        <div className={'apply-button'}>
              						<Button className='button button-primary margin-top' events={{onClick: this.handleClick.bind(this)}}>Apply</Button>
              					</div>
                    </div>
                  :
                  null
              }
              {
                   this.hasInvoices() ?
                      <DrilldownTable
                          cols={this.getColumns()}
                          source={this.getInvoices()}
                          rowHighlightColor="#C5E0B4"
                          rowHighlightCondition={this.rowHighlightCondition} />
                  :
                    'No invoices found'
              }
            </div>
      )
    }
}

export default ContributionInvoices
