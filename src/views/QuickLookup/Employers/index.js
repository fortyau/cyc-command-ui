import React from 'react'
import Card from 'components/Card'
import PageTitle from 'components/PageTitle'
import SectionTitle from 'components/SectionTitle'
import SearchBox from 'components/SearchBox'
import EmployerData from 'data/employers'
import Loading from 'components/Loading'
import DrilldownTable from 'components/DrilldownTable'
import QuickLookupStore from 'stores/QuickLookupStore'
import {quickLookupStoreActions} from 'stores/QuickLookupStore'
import {snackbarActions} from 'stores/SnackbarStore'

class EmployerSearch extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          searchText: QuickLookupStore.state.search.text,
          results: [],
          searching: false
      };
    }

    componentWillMount() {
      const searchText = this.state.searchText;
      if(searchText) {
        quickLookupStoreActions.clearSearch();
        this.handleSearchSubmit()
      }
    }

    handleSearchSubmit() {
      const { searchText } = this.state;
      if(searchText) {
        this.setState({searching: true, results: []});
        new EmployerData()
            .getEmployers("employer", searchText)
            .then(data => {
                this.setState({
                  searching: false,
                  results: data });

                if(data.length === 0) {
                  snackbarActions.showSnackbar('No employers found', 'info');
                }
            });
        } else {
          snackbarActions.showSnackbar('Employer ID or name required', 'info');
        }
    }

    handleSearchChange(searchText) {
      this.setState({
        searchText
      });
    }

    buildLink(obj) {
      return `/employers/${obj.employer.id}`;
    }

    getColumns() {
      return [
          {
              header: '',
              isLink: true,
              getText: (rawObj) => 'Select'
          },
          {
              header: 'Employer',
              getText: (rawObj) => rawObj.employer.name
          },
          {
              header: 'Distributor',
              getText: (rawObj) => rawObj.distributor.name
          },
          {
              header: 'Sponsor',
              getText: (rawObj) => rawObj.sponsor.name
          }
      ];
    }

    render () {
        return (
          <Card>
              <PageTitle value="Find Employers by ID or Name"/>
              <div className="margin-top">
                  <SearchBox
                      placeholder='Enter ID or Name'
                      value={this.state.searchText}
                      onChange={this.handleSearchChange.bind(this)}
                      onSubmit={this.handleSearchSubmit.bind(this)}
                  />
                  <SectionTitle value="Search Results" />
                  <DrilldownTable
                    pageSize={10}
                    buildLink={this.buildLink}
                    cols={this.getColumns()}
                    source={this.state.results} />
                  { this.state.searching ? <Loading /> : null }
              </div>
          </Card>
        )
    }
}

export default EmployerSearch
