import React from 'react'
import DrilldownTable from 'components/DrilldownTable'
import PaymentCardPaymentViewModel from 'viewModels/paymentCardPaymentViewModel'
import Card from "../../components/Card";
import PageTitle from "../../components/PageTitle";
import SectionTitle from "../../components/SectionTitle";
import TextField from "../../components/TextField";

import './Providers.scss'
import ProvidersData from "../../data/providers";
import Loading from "../../components/Loading";
import ClaimsData from "../../data/claims";

const pmtCols =
    [{
        header: 'Payment Number',
        isLink: true,
        property: 'linkText'
    }, {
        header: 'Reimbursement Method',
        property: 'reimbursementMethod'
    }, {
        header: 'Paid Date',
        property: 'paidDate',
    }, {
        header: 'Cleared Date',
        property: 'clearedDate',
    }, {
        header: 'Payment Status',
        property: 'paymentStatus'
    }];

const claimCols =
    [{
        header: 'Charge Id',
        property: 'claimId',
        isLink: true
    }, {
        header: 'Claim Number',
        property: 'submissionId'
    }, {
        header: 'Claim Type',
        property: 'claimType',
    }, {
        header: 'Claim Status',
        property: 'claimStatus'
    }];


class Providers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            providerId: props.match.params.id,
            loading: true,
            provider: {},
            payments: [],
            paymentCards: [],
            externalTransfers: [],
            relatedClaims: []
        };
    }

    componentDidMount() {

        const providersData = new ProvidersData();
        const claimsData = new ClaimsData();

        const loadProvider = providersData.getProvider(this.state.providerId)
            .then(data => this.setState({provider: data}));

        const loadPaymentCards = providersData.getPaymentCards(this.state.providerId)
            .then(data => this.setState({paymentCards: data.map(x => new PaymentCardPaymentViewModel(x))}));

        const loadRelatedClaims = claimsData.getClaimsRelatedToProvider(this.state.providerId)
            .then(data => this.setState({relatedClaims: data}));

        Promise.all([loadPaymentCards, loadRelatedClaims, loadProvider])
            .then(() => this.setState({payments: this.state.paymentCards, loading: false}))
    }

    buildLink = claim => '/claims/' + claim.claimId;

    renderContents = () => {
        return (
            <div>
                <PageTitle value={`Provider # ${this.state.provider.id}`}/>
                <div className="provider-detail-row">
                    <TextField label="Name" value={this.state.provider.name}/>
                    <TextField label="Opted Out" value={this.state.provider.optOut ? "Yes" : "No"}/>
                    <TextField label="Address" value={`${this.state.provider.address1} ${this.state.provider.city} ${this.state.provider.state}, ${this.state.provider.zipCode}`}/>
                </div>
                <SectionTitle value="Related Claims"/>
                <DrilldownTable
                    cols={claimCols}
                    source={this.state.relatedClaims}
                    buildLink={this.buildLink}
                    paginationEnabled={true}
                    pageSize={10}
                />
                <SectionTitle value="Related Payments"/>
                <DrilldownTable
                    cols={pmtCols}
                    source={this.state.payments}
                    buildLink={(x) => x.linkUrl}
                    paginationEnabled={true}
                    pageSize={10}
                />
            </div>
        )
    };

    render() {
        return (
            <Card className="margin-top">
                {this.state.loading ?
                    <Loading/> :
                    this.renderContents()}
            </Card>
        )
    }

}

export default Providers;