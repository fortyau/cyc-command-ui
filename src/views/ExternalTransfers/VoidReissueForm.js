import React from 'react'
import Field from 'components/forms/Field'

class VoidReissueForm extends React.Component {

    checkStatusDropdown = {
        name: 'Updating Check Status to: ',
        type: 'dropdown',
        placeholder: 'Void',
        events: {
            onChange: this.props.statusChangeHandler
        },
        data: {
            // None: {
            //     name: 'None',
            //     disabled: true
            // },
            Void: {
                name: 'Void',
                disabled: this.props.checkStatus === 'VOID'
            },
            Reissue: {
                name: 'Reissue',
                disable: false
            },
            // Rejected: {
            //     name: 'Rejected',
            //     disable: false
            // },
            //
            // StopCheck: {
            //     name: 'Stop Check',
            //     disable: false
            // }
        }
    }

    secondaryFields = {
        Void: {
            name: 'Reason for Status Change: ',
            type: 'dropdown',
            placeholder: 'Other',
            events: {onChange: this.props.voidReasonHandler},
            data: {
                Other: {
                    name: 'Other',
                    disabled: false
                }
            }
        },
        Rejected: {
            name: 'Rejected Reason: '
        }
    }

    render() {
        const {checkStatus, voidReason, submitHandler, voidOtherHandler, voidOtherReason} = this.props
        return (
            <form onSubmit={submitHandler}>
                <fieldset>
                    <Field {...this.checkStatusDropdown}/>
                    {this.secondaryFields[checkStatus] ?
                        <Field {...this.secondaryFields[checkStatus]}/> :
                        null}
                    {voidReason === 'Other' &&
                    checkStatus === 'Void' ?
                        <Field name="Reason for Status of Other: " events={{onChange: voidOtherHandler}} type="text" value={voidOtherReason}/> :
                        null}
                </fieldset>
                <button type="submit" className="button button-inline button-edit">Submit</button>
            </form>
        )
    }
}

export default VoidReissueForm