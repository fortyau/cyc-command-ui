import React from 'react'

import SectionTitle from 'components/SectionTitle'
import Loading from 'components/Loading'
import TextField from 'components/TextField'
import ButtonBar from 'components/ButtonBar'

import './PayeeAddress.scss'

class PayeeAddress extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
        editing: false, 
        saving: false, 
        success: false, 
        providerName: props.address.name,
        address1: props.address.address1,
        address2: props.address.address2,
        city: props.address.city,
        state: props.address.state,
        zipCode: props.address.zipCode,
        saved: false
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.revert = this.revert.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getActions = this.getActions.bind(this);
  }

  renderReadOnly() {
    return (
    <div>
      <div className="address-detail-row">
        <div style={{flexBasis: '20%'}}>
            <TextField label="Provider Name" value={this.state.providerName} />
        </div>
        <div style={{flexBasis: '20%'}}>
            <TextField label="Address 1" value={this.state.address1} />
        </div>
        <div style={{flexBasis: '20%'}}>
            <TextField label="Address 2" value={this.state.address2} />
        </div>
        <div style={{flexBasis: '10%'}}>
            <TextField label="City" value={this.state.city} />
        </div>
        <div style={{flexBasis: '4%'}}>
            <TextField label="State" value={this.state.state} />
        </div>
        <div style={{flexBasis: '6%'}}>
            <TextField label="Zip Code" value={this.state.zipCode} />
        </div>
      </div>
      {this.props.editable ? <ButtonBar actions={this.getActions()} onClick={this.handleClick} toggle={this.state.editing} /> : null}
      </div>
      )
  }
  
  getActions() {
      let actions = ['Edit'];
      if (this.state.saved  && this.props.canVoidReissue) {
          actions.push('Reissue Check')
      }
      return actions
  }

  renderEditable() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="address-detail-row">
                    <TextField label="Provider Name" ref="provName" value={this.state.providerName} onChange={(event) => this.handleChange('providerName', event)}
                        editable={true} size={40} required={true} maxLength={50} />
                    <TextField label="Address 1" value={this.state.address1} onChange={(event) => this.handleChange('address1', event)}
                        editable={true} size={40} required={true} maxLength={40} />
                    <TextField label="Address 2" value={this.state.address2} onChange={(event) => this.handleChange('address2', event)}
                        editable={true} size={40} maxLength={40} />
                    <TextField label="City" value={this.state.city} onChange={(event) => this.handleChange('city', event)}
                        editable={true} size={20} required={true} maxLength={20} />
                    <TextField label="State" value={this.state.state} onChange={(event) => this.handleChange('state', event)}
                        editable={true} size={6} required={true} maxLength={2} minLength={2} pattern='^(?:A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[AT]|W[AIVY])*$' />
                    <TextField label="Zip Code" value={this.state.zipCode} onChange={(event) => this.handleChange('zipCode', event)}
                        editable={true} size={12} required={true} maxLength={10} minLength={5} pattern='^([0-9]{5})-?(?:[0-9]{4})?$' />
                </div>
                <ButtonBar actions={['Edit']} onClick={this.handleClick} toggle={this.state.editing} />
            </form>
        )
  }

  handleChange(key, val) {
    const newState = {};
    newState[key] = val;
    this.setState(newState);
  }

  handleSubmit() {
    this.handleClick('save');
  }

  handleClick(action)  {
    if (action === 'cancel') {
        this.setState({editing: false});
        this.revert();
    }
    else if (action === 'save') {
        this.props.onSave({
            name: this.state.providerName,
            address1: this.state.address1,
            address2: this.state.address2,
            city: this.state.city,
            state: this.state.state,
            zipCode: this.state.zipCode
          });
        this.setState({editing: false, saved: true});
    }
    else if (action === "Edit") {
        this.setState({editing: true});
    }
    else if (action === "Reissue Check") {
        this.props.onReissue();
    }
  }

  revert() {
    this.setState({
        providerName: this.props.address.name,
        address1: this.props.address.address1,
        address2: this.props.address.address2,
        city: this.props.address.city,
        state: this.props.address.state,
        zipCode: this.props.address.zipCode
    });
  }

  render () {
    return (
        <div>
            <SectionTitle value="Payee Address" />
            {this.state.saving ? <Loading />
            : <div>
                {this.state.success ? <div className="success-message">Payee Address Changed Successfully</div> : null}
                {!this.props.editable || !this.state.editing ? this.renderReadOnly() : this.renderEditable()}
            </div>}
        </div>
    )
  }
}

export default PayeeAddress