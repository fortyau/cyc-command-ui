import React from 'react'
import Card from 'components/Card'

import PageTitle from 'components/PageTitle'
import SectionTitle from 'components/SectionTitle'
import Loading from 'components/Loading'
import TextField from 'components/TextField'
import DrilldownTable from 'components/DrilldownTable'
import Modal from 'components/Modal'
import VoidReissueForm from './VoidReissueForm'
import PayeeAddress from './PayeeAddress'
import ClaimsData from 'data/claims'
import ExternalTransfersData from 'data/externalTransfers'
import ProviderData from 'data/providers'
import Formatters from 'data/formatters'
import {snackbarActions} from 'stores/SnackbarStore'
import Security from 'data/security'

import './ExternalTransferDetails.scss'

const claimCols = [
    {
        header: 'Charge Id',
        property: 'claimId',
        isLink: true
    },
    {
        header: 'Claim Number',
        property: 'submissionId'
    }, {
        header: 'Claim Type',
        property: 'claimType',
    }
];

class ExternalTransferDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            externalTransferId: props.match.params.id,
            externalTransfer: {},
            loading: true,
            addressLoading: false,
            modal: false,
            editing: true,
            addressSuccess: false,
            editingNew: false,
            relatedClaims: [],
            checkStatus: 'Void',
            voidReason: 'Other',
            voidOtherReason: '',
            voidReissueLoading: false,
            externalTransferLoading: false
        };
        this.openModal = this.openModal.bind(this);
        this.onSaveAddress = this.onSaveAddress.bind(this);
        this.onReissue = this.onReissue.bind(this);
    }

    componentDidMount() {
        this.setState({loading: true});

        const claimData = new ClaimsData();

        const getClaims = claimData.getClaimsRelatedToExternalTransfer(this.state.externalTransferId)
            .then(data => this.setState({relatedClaims: data}));

        const getExternalTransfer = this.fetchExternalTransfer();

        Promise.all([getClaims, getExternalTransfer])
            .then((data) => this.setState({loading: false}));
    }

    fetchExternalTransfer() {
        const externalTransfersData = new ExternalTransfersData();

        return externalTransfersData.getExternalTransfer(this.state.externalTransferId)
            .then(data => this.setState({externalTransfer: data}));
    }

    onModalClose() {
        this.setState({modal: false, checkStatus: 'Void', voidReason: 'Other'})
    }

    openModal() {
        this.setState({modal: true})
    }

    onSaveAddress(address) {
        this.setState({addressLoading: true});

        const providerData = new ProviderData();

        providerData.saveAddress(this.state.externalTransfer.provider.id, address)
            .then(data => {
                if (data) {
                    var externalTransfer = this.state.externalTransfer;
                    externalTransfer.provider.name = data.name;
                    externalTransfer.provider.address1 = data.address1;
                    externalTransfer.provider.address2 = data.address2;
                    externalTransfer.provider.city = data.city;
                    externalTransfer.provider.state = data.state;
                    externalTransfer.provider.zipCode = data.zipCode;
                    this.setState({addressLoading: false, externalTransfer: externalTransfer});
                    snackbarActions.showSnackbar('Address Updated', 'info');
                }
            });
    }

    onReissue() {
        this.setState({voidReissueLoading: true});
        const externalTransferData = new ExternalTransfersData();
        return externalTransferData.voidOrReissueExternalTransfer(this.state.externalTransfer.id, this.issueTypeLookup['Reissue'], '')
            .then(response => {
                if (response) {
                    this.setState({voidReissueLoading: false, modal: false})
                    snackbarActions.showSnackbar('Check Reissued', 'info')
                    this.fetchExternalTransfer();
                }
            })
    }

    canEdit() {
        const sec = new Security();
        return sec.hasEveryRole(['CYCCEdit', 'claimssupervisor']);
    }

    canVoidReissue() {
        const sec = new Security();
        return sec.hasEveryRole(['CYCCEdit', 'claimssupervisor', 'CSRCheckReissue'])
    }

    buildLink(claim) {
        return '/claims/' + claim.claimId;
    }

    getExternalTransferTypeName(id) {
        const types = {
            363: 'ACH',
            364: 'Check',
            365: 'PayrollDeduction',
            366: 'DirectTransfer',
            367: 'PaySpanPayment'
        };

        return types[id];
    }

    issueTypeLookup = {
        'Void': 12401,
        'Reissue': 12402
    };

    handleStatusDropdownChange = e => this.setState({checkStatus: e});
    handleVoidReasonDropdownChange = e => this.setState({voidReason: e});
    handleVoidOtherChange = e => this.setState({voidOtherReason: e});

    handleVoidReissueSubmission = (e) => {
        e.preventDefault();
        this.setState({voidReissueLoading: true});
        const externalTransferData = new ExternalTransfersData();
        return externalTransferData.voidOrReissueExternalTransfer(this.state.externalTransfer.id, this.issueTypeLookup[(this.state.checkStatus)], this.state.voidOtherReason)
            .then(response => {
                if (response) {
                    this.setState({voidReissueLoading: false, modal: false});
                    snackbarActions.showSnackbar('Payment Updated', 'info');
                    this.fetchExternalTransfer();
                }
            })
    };

    renderCheck() {
        return (
            <div>
                <SectionTitle value="Check Details"/>
                <div className="check-detail-row">
                    <TextField label="Check Number" value={this.state.externalTransfer.checkNumber}/>
                    <TextField label="Issued Date" value={this.state.externalTransfer.issueDate}/>
                    <TextField label="Check Amount" value={this.state.externalTransfer.amount} formatter={Formatters.currency}/>
                    <TextField label="Check Status" value={this.state.externalTransfer.paymentStatus}/>
                </div>
                <div className="check-detail-row">
                    <TextField label="Reject Reason" value={this.state.externalTransfer.rejectReason}/>
                    <div className="check-detail-triple"><TextField label="Notes" value={this.state.externalTransfer.notes}/>
                    </div>
                </div>
                {this.canVoidReissue() ?
                    <div className="margin-top">
                        <button className="button button-inline button-edit" onClick={this.openModal}>Check Void/Reissue</button>
                    </div>
                    : null}
                {this.state.externalTransfer.provider &&
                    <PayeeAddress
                        address={this.state.externalTransfer.provider}
                        onSave={this.onSaveAddress}
                        onReissue={this.onReissue}
                        editable={this.canEdit()}
                        canVoidReissue={this.canVoidReissue()}/>}
            </div>
        );
    }

    renderNonCheck() {
        return (
            <div>
                <SectionTitle value={`${this.getExternalTransferTypeName(this.state.externalTransfer.typeId)} Details`}/>
                <div className="check-detail-row">
                    <TextField label="Number" value={this.state.externalTransfer.journalId}/>
                    <TextField label="Issued Date" value={this.state.externalTransfer.issueDate}/>
                    <TextField label="Amount" value={this.state.externalTransfer.amount} formatter={Formatters.currency}/>
                    <TextField label="Status" value={this.state.externalTransfer.paymentStatus}/>
                </div>
                <div className="check-detail-row">
                    <TextField label="Reject Reason" value={this.state.externalTransfer.rejectReason}/>
                    <div className="check-detail-triple"><TextField label="Notes" value={this.state.externalTransfer.notes}/>
                    </div>
                </div>
                {this.state.externalTransfer.provider ?
                    <PayeeAddress address={this.state.externalTransfer.provider} editable={false}/> :
                    null}
            </div>
        );
    }

    renderTitle() {
        return (<PageTitle
            value={`${this.getExternalTransferTypeName(this.state.externalTransfer.typeId)} #${this.state.externalTransfer.typeId === 364 ? this.state.externalTransfer.checkNumber : this.state.externalTransfer.journalId}`}/>);
    }

    renderContents() {
        return (
            <div className="margin-top">
                <div>This payment included the following claims:</div>
                <DrilldownTable
                    cols={claimCols}
                    source={this.state.relatedClaims}
                    buildLink={this.buildLink}
                    paginationEnabled={true}
                    pageSize={10}
                />

                {this.state.voidReissueLoading ? 
                    <Loading />
                    : this.state.externalTransfer.typeId === 364 ? this.renderCheck() : this.renderNonCheck()}
            </div>)
    }

    renderModal() {
        return (
            <Modal
            header="Check Void / Reissue"
            onClose={this.onModalClose.bind(this)}
            showClose={true}>
            {this.state.voidReissueLoading ? 
            <Loading /> 
            : <div>
                <DrilldownTable
                    cols={claimCols}
                    source={this.state.relatedClaims}
                    buildLink={this.buildLink}
                    paginationEnabled={true}
                    pageSize={5}
                    />
                <VoidReissueForm
                    voidReasonHandler={this.handleVoidReasonDropdownChange}
                    statusChangeHandler={this.handleStatusDropdownChange}
                    submitHandler={this.handleVoidReissueSubmission}
                    voidOtherHandler={this.handleVoidOtherChange}
                    checkStatus={this.state.checkStatus}
                    voidReason={this.state.voidReason}
                    voidOtherReason={this.state.voidOtherReason}
                    loading={this.state.voidReissueLoading}/>
            </div>}
        </Modal>)
    }

    render() {
        return (
            <Card>
                {this.state.loading ?
                    <Loading/>
                    : <div>
                        {this.renderTitle()}
                        {this.renderContents()}
                    </div>}
                {this.state.modal ? this.renderModal() : null}
            </Card>
        )
    }
}

export default ExternalTransferDetails