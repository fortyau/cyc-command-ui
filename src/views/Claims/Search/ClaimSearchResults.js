import React from 'react'
import Loading from "../../../components/Loading";
import DrilldownTable from "../../../components/DrilldownTable";
import Formatters from "../../../data/formatters";

const cols = [
	{
		header: 'Claim #',
		property: 'submissionId'
	},
	{
		header: 'Charge #',
		property: 'claimId',
		isLink: true
	},
	{
		header: 'Claim Date',
		property: 'serviceDate'
	},
	{
		header: 'Claim Type',
		property: 'claimType'
	},
	{
		header: 'Vendor/Provider',
		property: 'vendor'
	},
	{
		header: 'Employee Name',
		getText: (rawObj) => Formatters.lastCommaFirst(rawObj.employeeLastName, rawObj.employeeFirstName)
	},
	{
		header: 'Employee SSN',
		getText: (rawObj) => Formatters.ssnLast4(rawObj.employeeSsnLastFour)
	},
	{
		header: 'Claim Amt',
		getText: (rawObj) => Formatters.currency(rawObj.claimAmount)
	},
	{
		header: 'Paid Amount',
		getText: (rawObj) => Formatters.currency(rawObj.invoiceAmount)
	},
	{
		header: 'Status',
		property: 'claimStatus'
	}
];

class ClaimSearchResults extends React.Component {

	buildLink(claim) {
		return '/claims/' + claim.claimId;
	}

	render() {
		const {claims, hasSearched, loading} = this.props;
		return (
			<div>
				{loading ?
					<Loading/> :
					claims && claims.length > 0 ?
						<div>
							<DrilldownTable
								cols={cols}
								source={claims}
								buildLink={this.buildLink}/>
						</div> :
						hasSearched && <div>No Claims Found</div>}
			</div>
		)
	}
}

export default ClaimSearchResults