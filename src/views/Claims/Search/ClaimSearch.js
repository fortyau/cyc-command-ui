import React from 'react';
import Field from "../../../components/forms/Field";
import Button from "../../../components/Button";

import './ClaimSearch.scss'

const claimSearchFields = {
	fields: {
		claimId: {
			name: 'claimId',
			label: 'Claim ID',
			placeholder: '',
			type: 'text',
			validator: null,
			required: false
		},
		ssn: {
			name: 'ssn',
			label: 'SSN',
			placeholder: '',
			type: 'text',
			validator: null,
			required: false
		}
	}
};

class ClaimSearch extends React.Component {

	getClaimSearchFields() {

		const {fields} = claimSearchFields;

		return Object.entries(fields).map(field => {
			const [, values] = field;
			return {
				value: this.props.form[values.name] ? this.props.form[values.name] : "",
				...values,
				events: {
					onChange: event => this.props.handleChange(values.name, event.target.value.trim())
				}
			}
		})
	}

	render() {
		return (
			<form style={{'display': 'flex', 'flexFlow': 'row wrap'}}
						onSubmit={this.props.handleSubmit}>
				{this.getClaimSearchFields().map((f, idx) => <Field className={'search-input'} key={`${f.name}-${idx}`} {...f}/>)}
				<div className={'form-formatting'}>
					<Button disabled={this.props.submitting} type='submit'
									className='button button-primary margin-top'>Search</Button>
				</div>
			</form>
		)
	}

}

export default ClaimSearch