import React from 'react'

import SectionTitle from 'components/SectionTitle'
import DrilldownTable from 'components/DrilldownTable'

class PaymentTable extends React.Component {
  constructor (props) {
    super (props);
    this.state = {};
  }

  getPmtCols() {
    return [
      {
        header: 'Payment Number',
        isLink: true,
        property: 'linkText'
      },{
        header: 'Reimbursement Method',
        property: 'reimbursementMethod'
      },{
        header: 'Paid Date',
        property: 'paidDate',
      },{
        header: 'Cleared Date',
        property: 'clearedDate',
      },{
        header: 'Paid To',
        property: 'paidTo'
      },{
        header: 'Address',
        property: 'address'
      }
    ];
  }

  render () {
    return (
      <div className="margin-top">
        <SectionTitle value="Payments" />
        {this.props.source.length > 0 ?
          <DrilldownTable 
            cols={this.getPmtCols()}
            source={this.props.source}
            buildLink={(x) => x.linkUrl}
            />
          : <div className="soft-text">none</div>}
      </div>
    )
  }
}

export default PaymentTable
