import React from 'react'
import Card from 'components/Card'

import PageTitle from 'components/PageTitle'
import Loading from 'components/Loading'
import TextField from 'components/TextField'
import ButtonBar from 'components/ButtonBar'
import PaymentTable from '../Payments/Table'
import DateField from 'components/DateField'
import DropdownField from 'components/DropdownField'
import NumberField from 'components/NumberField'
import Button from "components/Button"
import Modal from 'components/Modal'

import ClaimsData from 'data/claims'
import ProviderPaymentCardsData from 'data/providerPaymentCards'
import ExternalTransfersData from 'data/externalTransfers'
import PeopleData from 'data/people'
import ServiceTypesData from 'data/serviceTypes'
import RejectReasonsData from 'data/rejectReasons'
import ClaimStatusData from 'data/claimStatuses'

import ProvidersData from "../../../data/providers";
import StatusService from './statusService'
import PaymentCardPaymentViewModel from 'viewModels/paymentCardPaymentViewModel'
import ExternalTransferPaymentViewModel from 'viewModels/externalTransferPaymentViewModel'
import Formatters from '../../../data/formatters'
import {snackbarActions} from 'stores/SnackbarStore'
import {recentClaimActions} from 'stores/RecentClaimStore';
import Security from 'data/security'

import './ClaimDetails.scss'

class ClaimDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            claimId: props.match.params.id,
            claim: {},
            payments: [],
            externalTransfers: [],
            paymentCards: [],
            loading: true,
            editing: false,
            approvingClaim: false,
            changes: {},
            canBeOptedOut: false,
            optOutModal: false
        };
        this.renderContents = this.renderContents.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.saveClaim = this.saveClaim.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.startEdit = this.startEdit.bind(this);
        this.openOptOutModal = this.openOptOutModal.bind(this);
        this.closeOptOutModal = this.closeOptOutModal.bind(this);
    }

    componentDidMount() {
        this.setState({loading: true});

        let loadSteps = [];

        recentClaimActions.setRecentClaim(this.state.claimId)

        const claimsData = new ClaimsData();
        loadSteps.push(claimsData.getClaimDetails(this.state.claimId)
            .then(data => {
                this.setState({claim: data});
            }));

        const externalTransferData = new ExternalTransfersData();
        loadSteps.push(externalTransferData.getExternalTransfersForClaim(this.state.claimId)
            .then(data => {
                this.setState({
                    externalTransfers:
                        data.filter(x => [363, 364].includes(x.typeId))
                            .map(x => new ExternalTransferPaymentViewModel(x))
                });
            }));

        if (process.env.REACT_APP_PPC_FEATURE_TOGGLE) {
            const providerPaymentCardsData = new ProviderPaymentCardsData();
            loadSteps.push(providerPaymentCardsData.getProviderPaymentCardsForClaim(this.state.claimId)
                .then(data => {
                    // check if any payments came back from query
                    if (data.length > 0) {
                        this.setState({paymentCards: data.map(x => new PaymentCardPaymentViewModel(x))});
                    }
                }));
        }

        const peopleData = new PeopleData();
        loadSteps.push(peopleData.getDependentsForClaim(this.state.claimId)
            .then(data => {
                this.setState({dependents: data});
            }));

        const serviceTypesData = new ServiceTypesData();
        loadSteps.push(serviceTypesData.getAll()
            .then(data => {
                this.setState({serviceTypes: data});
            }));

        const claimStatusData = new ClaimStatusData();
        loadSteps.push(claimStatusData.getAll()
            .then(data => {
                this.setState({claimStatuses: data});
            }));

        const rejectReasonsData = new RejectReasonsData();
        loadSteps.push(rejectReasonsData.getAll()
            .then(data => {
                this.setState({rejectReasons: data});
            }));

        Promise.all(loadSteps)
            .then(_ => {

                const {paymentCards} = this.state;
                let optOut = false;

                if (paymentCards.length > 0) {
                    const cardDict = paymentCards.reduce((acc, {paymentStatus, linkText}) => {
                        acc[linkText] = acc[linkText] ? [...acc[linkText], paymentStatus] : [paymentStatus];
                        return acc
                    }, {});
                    //
                    const terminatedStatuses = ["Voided", "To be deleted", "Deleted"];
                    // // First condition checks that a terminated status exists in every card status arrays
                    // // Second condition checks that there is a provider id associated with the claim
                    // // Third condition looks at current provider opt out status
                    optOut = Object.values(cardDict)
                            .every(statusArray =>
                                statusArray.some(status => terminatedStatuses.includes(status)))
                        && (this.state.claim.providerId !== null)
                        && !this.state.claim.providerOptOut;
                }

                const payments = this.state.externalTransfers.concat(this.state.paymentCards);
                this.setState({payments: payments, allowedStatuses: this.getAllowedStatuses(), loading: false, canBeOptedOut: optOut});
            });
    }

    getAllowedStatuses() {
        return new StatusService().getAllowedStatuses(this.state.claim, this.state.claimStatuses)
    }

    loadDetails() {
        const data = new ClaimsData();
        this.setState({loading: true});
        return data.getClaimDetails(this.state.claimId)
            .then(data => {
                this.setState({claim: data, allowedStatuses: this.getAllowedStatuses(), approvingClaim: false, loading: false});
            });
    }

    saveClaim() {
        this.setState({editing: false, loading: true});
        const changed = this.state.changes;
        let changes = [];
        for (var property in changed) {
            if (changed.hasOwnProperty(property)) {
                var value = changed[property];
                if (!value) {
                    value = null;
                }
                changes.push({path: property, value: value})
            }
        }
        new ClaimsData().update(this.state.claimId, changes)
            .then(_ => {
                snackbarActions.showSnackbar('Claim Saved. Refreshing...', 'info')
                return this.loadDetails();
            })
    }

    handleSubmit(e) {
        this.saveClaim();
        if (e) {
            e.preventDefault();
        }
    }

    handleChange(key, val, apiVal) {
        var claim = {...this.state.claim, [key]: val};
        let updatedChangeObject = {...this.state.changes, [key]: String(apiVal || val)};
        var rejectTypeRequired = this.state.rejectTypeRequired;
        if (key !== 'rejectType') {
            rejectTypeRequired = [
                5, /*Ineligible*/
                8, /*Paid, Documents Needed*/
                10, /*Ineligible, Repayment Required*/
                15, /*Ineligible, Repayment Required*/
                16, /*Partially Approved, Repayment Required*/
                18, /*Partially Approved*/
            ].includes(claim.status.id);
            if (rejectTypeRequired) {
                claim.rejectType = this.state.rejectReasons[0];
                updatedChangeObject.rejectType = claim.rejectType.id;
            }
            else {
                claim.rejectType = null;
                updatedChangeObject.rejectType = null;
            }
        }
        var state = {claim, changes: updatedChangeObject, rejectTypeRequired};
        this.setState(state);
        console.log(state);
    }

    startEdit() {
        this.setState({
            backup: {...this.state.claim}, claim: {...this.state.claim, invoiceAmount: this.state.claim.amount},
            editing: true, changes: {invoiceAmount: this.state.claim.amount}
        });
    }

    cancelEdit() {
        this.setState({claim: {...this.state.backup}, backup: null, changes: {}, editing: false});
    }

    handleClick(action) {
        if (action === 'cancel') {
            this.cancelEdit();
        }
        else if (action === 'save') {
            this.saveClaim();
        }
        else if (action === "Edit Claim") {
            this.startEdit();
        }
        else if (action === "Approve Claim") {
            this.setState({approvingClaim: true});
            const {claimId} = this.state.claim;
            const payload = {statusId: 6, approvedAmount: this.state.claim.amount};
            const claimsData = new ClaimsData();
            claimsData.approveClaim(claimId, payload)
                .then(response => {
                    snackbarActions.showSnackbar('Claim Approved', 'info')
                    this.loadDetails(claimId);
                });
        }
        else if (action === "Opt Out") {
            this.openOptOutModal();
        }
    }

    getAccountLink() {
        if (!this.state.claim.accounts || this.state.claim.accounts.length === 0) {
            return {text: '', url: ''};
        }

        // todo: handle multiples
        return {
            text: this.state.claim.accounts[0].name,
            url: `/accounts/${this.state.claim.accounts[0].id}`
        }
    }

    getProviderLink = () => ({text: this.state.claim.vendor, url: `/providers/${this.state.claim.providerId}`});

    getActions() {
        let actions = [];

        const sec = new Security();

        if (sec.hasRole('CYCCEdit')) {
            if (!this.state.editing) {
                const approvableStatuses = [
                    4,  // Approved, Processing Reimbursement
                    3,  // Under Review
                    44, // Closed
                ];
                if (this.state.canBeOptedOut && sec.hasEveryRole(['CYCCEdit', 'claimssupervisor', 'CSRCheckReissue'])) {
                    actions.push('Opt Out');
                }
                // 41 (Closed) is locked down. No Edits allowed at all!
                if (this.state.claim.status.id !== 41) {
                    if (approvableStatuses.includes(this.state.claim.status.id) && sec.hasEveryRole(['CYCCEdit', 'claimssupervisor'])) {
                        actions.push('Approve Claim');
                    }
                    if (sec.hasAtLeastOneRole(['claimssupervisor', 'claimedit'])) {
                        actions.push('Edit Claim');
                    }
                }
            }
        }
        return actions;
    }

    openOptOutModal = () => this.setState({optOutModal: true});
    closeOptOutModal = () => this.setState({optOutModal: false});
    handleOptOut = () => {
        const providerData = new ProvidersData();

        providerData.denyPamentCards(this.state.claim.providerId)
            .then(data => {
                this.setState({optOutModal: false, canBeOptedOut: false});
                snackbarActions.showSnackbar('Opt Out Successful', 'info');
            })
    };

    renderOptOutModal() {
        return (
            <Modal
                header="Provider Opt Out"
                onClose={this.closeOptOutModal}
                showClose={true}>
                <div className="margin-top">
                    By pressing Submit, you will be updating this Provider to be opted out of Provider Payments for this and future claims.
                </div>
                <div>
                    <Button events={{onClick: this.handleOptOut}} className='button button-primary margin-top margin-right'>Submit</Button>
                    <Button events={{onClick: this.closeOptOutModal}} className='button button-secondary margin-top margin-right'>Cancel</Button>
                </div>
            </Modal>)
    }

    renderContents() {
        const editable = this.state.editing;
        return (
            <form onSubmit={_ => this.handleSubmit()}>
                <div>
                    <div className="claim-detail-row">
                        {editable ?
                            <DateField value={this.state.claim.dateOfService} label="Date of Service" onChange={e => this.handleChange('dateOfService', e)}/>
                            : <TextField label="Date of Service" name="dateOfService" value={this.state.claim.dateOfService} required={true}/>}
                        <TextField label="Claim Creation Date" value={this.state.claim.dateCreated}/>
                        {editable ?
                            <DateField value={this.state.claim.receiptDate || ''} label="Claim Receipt Date" onChange={e => this.handleChange('receiptDate', e)}/>
                            : <TextField label="Claim Receipt Date" name="receiptDate" value={this.state.claim.receiptDate ? this.state.claim.receiptDate : ''}/>}
                        {editable ?
                            <DropdownField options={this.state.dependents} formatter={Formatters.person} label={'Service For'}
                                           valuePath={"id"} keyPath={'id'} value={this.state.claim.serviceFor.id} onChange={e => this.handleChange('serviceFor', e, e.id)}/>
                            : <TextField label="Service For" name="serviceFor" value={this.state.claim.serviceFor} formatter={Formatters.person}/>}
                    </div>
                    <div className="claim-detail-row">
                        <TextField label="Vendor/Provider" value={this.state.claim.vendor} link={this.state.claim.providerId && this.getProviderLink()}
                                   onChange={e => this.handleChange('vendor', e)} editable={editable}/>
                        <TextField label="Description" name={'description'} value={this.state.claim.description ? this.state.claim.description : ''}
                                   onChange={e => this.handleChange('description', e)} editable={editable}/>
                        {editable ?
                            <DropdownField options={this.state.serviceTypes} label={'Service Type'} displayPath={'name'} groupByPath={'categoryName'}
                                           keyPath={'id'} value={this.state.claim.serviceType.id} valuePath={'id'} onChange={e => this.handleChange('serviceType', e, e.id)}/>
                            : <TextField label="Service Type" value={this.state.claim.serviceType.name}/>}
                        <TextField label="MCC Code" value={this.state.claim.mccCode}/>
                    </div>
                    <div className="claim-detail-row">
                        <TextField label="Claim Amt" value={this.state.claim.amount} formatter={Formatters.currency}/>
                        {editable ?
                            <NumberField value={this.state.claim.invoiceAmount} max={this.state.claim.amount} onChange={e => this.handleChange('invoiceAmount', e)}
                                         label="Approved Amt" editable={true} required={true} step={.01}/>
                            : <TextField label="Approved Amt" value={this.state.claim.invoiceAmount} formatter={Formatters.currency}/>}
                        <TextField label="Reimbursed Amt" value={this.state.claim.reimbursedAmount} formatter={Formatters.currency}/>
                        <TextField label="Not Yet Reimbursed" value={this.state.claim.notReimbursedAmount} formatter={Formatters.currency}/>
                    </div>
                    <div className="claim-detail-row">
                        {editable ?
                            <DropdownField options={this.state.allowedStatuses} label={'Status'} displayPath={'name'}
                                           keyPath={'id'} value={this.state.claim.status.id} valuePath={'id'} onChange={e => this.handleChange('status', e, e.id)}/>
                            : <TextField label="Status" value={this.state.claim.status.name}/>}
                        {editable && this.state.rejectTypeRequired ?
                            <DropdownField options={this.state.rejectReasons} label={'Rejection Reason'} displayPath={'name'}
                                           keyPath={'id'} value={this.state.claim.rejectType ? this.state.claim.rejectType.id : null} valuePath={'id'} onChange={e => this.handleChange('rejectType', e, e.id)}/>
                            : <TextField label="Rejection Reason" value={this.state.claim.rejectType ? this.state.claim.rejectType.name : 'Not Rejected'}/>
                        }
                        <TextField label="Reimbursement Void Reason" value={''}/>
                        <TextField label="Claim Origination Point" value={this.state.claim.originationPoint}/>
                    </div>
                    <div className="claim-detail-row">
                        <TextField label="Employee Name" value={Formatters.lastCommaFirst(this.state.claim.employee.lastName, this.state.claim.employee.firstName)}/>
                        <TextField label="Employee SSN" value={this.state.claim.employee.ssnLast4} formatter={Formatters.ssnLast4}/>
                        <TextField label="Claim Type" value={this.state.claim.claimType}/>
                        <TextField label="Account Type" value={this.state.claim.accountType} link={this.getAccountLink()}/>
                    </div>
                    <PaymentTable source={this.state.payments}/>
                    {this.state.approvingClaim ?
                        <Loading/>
                        : <ButtonBar actions={this.getActions()} toggle={this.state.editing} onClick={this.handleClick}/>}
                </div>
            </form>
        )
    }

    render() {
        return (
            <Card>
                {this.state.claim.submissionId ? <PageTitle value={'Claim # ' + this.state.claim.submissionId + ', Charge # ' + this.state.claimId}/> : null}
                <div>
                    {this.state.loading ?
                        <Loading/> :
                        this.renderContents()}
                    {this.state.optOutModal && this.renderOptOutModal()}
                </div>
            </Card>
        )
    }
}

export default ClaimDetails