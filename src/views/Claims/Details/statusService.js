class StatusService {
	getAllowedStatuses(claim, allStatuses) {
        // Controls the statuses the user can choose based on the current claim status and type.
        // Original list provided in Jira CYCC-298 (https://jira.connectyourcare.net/browse/CYCC-298)

        // Default map assuming that the logic below does not override
        var newStatusIdsAllowedByCurrentStatusId = {
            1: [1, 2],
            2: [2, 3],
            3: [3, 2, 5, 6, 18],
            4: [3, 4, 5],
            5: [5, 3, 9],
            6: [6, 4, 3],
            7: [7],
            8: [7, 8, 9],
            9: [7, 8, 15, 16],
            10:	[9, 6],
            43: [9, 6],
            45: [9, 6],
            46: [9, 6],
            15: [15, 21],
            16: [16, 17, 20],
            21:	[5, 6, 7, 8, 15, 16, 20],
            17: [7, 15, 16, 17, 20],
            18: [3, 18, 19],
            19: [19, 3],
            20: [3, 17, 20],
            34: [2, 5, 6, 18],
            44: [44, 6]
        };

        // Override for specific claim types / status
        if (claim.claimType === "Electronic Claim") {
            if (claim.status.id === 21) {
                newStatusIdsAllowedByCurrentStatusId[21] = [7, 8, 15, 16];
            }
            else if (claim.status.id === 23) {
                newStatusIdsAllowedByCurrentStatusId[23] = [21, 23];
            }
            else if (claim.status.id === 5) {
                newStatusIdsAllowedByCurrentStatusId[5] = [5, 9];
            }
        }
        else if (claim.claimType === "Manual Claim") {
            if (claim.status.id === 5) {
                newStatusIdsAllowedByCurrentStatusId[5] = [5, 3];
            }
        }

        // Select the allowed ids from the final map (after overrides)
        var allowedStatusIds = newStatusIdsAllowedByCurrentStatusId[claim.status.id] || [];
        if (!allowedStatusIds.includes(claim.status.id)) {
            // make sure we always have the current status so the dropdown will function
            allowedStatusIds.push(claim.status.id);
        }

        // Select the full status object based on which ids are allowed
        return allStatuses.filter(s => allowedStatusIds.includes(s.id));
	}
}

export default StatusService