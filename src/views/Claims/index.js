import React from 'react'
import Card from 'components/Card'
import PageTitle from 'components/PageTitle'
import ClaimsData from '../../data/claims'
import ClaimSearch from './Search/ClaimSearch'
import ClaimSearchResults from "./Search/ClaimSearchResults";
import {snackbarActions} from 'stores/SnackbarStore'

import 'components/forms/Field/field.scss'
import 'components/button.scss'

class Claims extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			claims: [],
			loading: false,
			hasSearched: false,
			form: {claimId: "", ssn: ""}
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(name, value) {
		let form = {...this.state.form};
		form[name] = value;
		this.setState({form})
	}

	handleSubmit(event) {
		this.setState({loading: true, hasSearched: true});
		event.preventDefault();

		if (this.state.form.claimId || this.state.form.ssn) {
			new ClaimsData()
				.getClaims(this.state.form.claimId, this.state.form.ssn)
				.then(data => this.setState({loading: false, claims: data}))
		} else {
			snackbarActions.showSnackbar('Please enter a value to search', 'info');
			this.setState({loading: false, hasSearched: false});
		}
	}

	render() {
		const {submitting, form, claims, hasSearched, loading} = this.state;
		return (
			<Card>
				<PageTitle value="Find a Claim" className={'form-formatting'}/>
				<ClaimSearch
					handleSubmit={this.handleSubmit}
					submitting={submitting}
					form={form}
					handleChange={this.handleChange}/>
				<ClaimSearchResults
					claims={claims}
					hasSearched={hasSearched}
					loading={loading}/>
			</Card>
		)
	}
}

export default Claims
