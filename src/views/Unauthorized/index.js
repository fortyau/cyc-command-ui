import React from 'react'

class Unauthorized extends React.Component {
    render () {
        return (
            <div>
                <h4 style={{textAlign: 'center'}}>Logged in User does not have access to CYC Command</h4>
            </div>
        )
    }
}

export default Unauthorized
