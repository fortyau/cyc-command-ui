import React from 'react'
import { Link } from 'react-router-dom'
import Card from 'components/Card'
import './index.scss'

const getOobLink = () => {
    let link = process.env.REACT_APP_OOBLINK;
    if (!link) {
        link = '';
    }
    return link;
}

const cards = [
    {
        header: "Claims",
        link: "/claims",
        items: [
            {name: "Process Payee Address Update", link: "/claims"},
            {name: "Process Reimbursement", link: "/claims"},
            {name: "Process Check Void / Reissue", link: "/claims"},
            {name: "Process Reverse Forfeiture", link: ""},
            {name: "Process Claim Transfer", link: ""},
            {name: "Schedule a Claim", link: ""},
            {name: "Process / Edit Claim", link: ""},
            {name: "Provider Payment Opt Out Tool", link: "/claims"}]
    }, {
        header: "Accounts",
        link: "/members",
        items: [
            {name: "Open Account", link: ""},
            {name: "Merge Accounts", link: ""},
            {name: "Close Account", link: ""},
            {name: "Process Pending HSA", link: ""},
            {name: "Update HSA Account Address/Demographics", link: ""},
            {name: "CIP Status", link: ""},
            {name: "Convert LP to GP", link: ""},
					  {name: "HSAB - Open/Close Account", link: "https://hsabank.force.com/partnersupport/login", external: true},
					  {name: "BNY - Open/Close Account", link: "https://sdm.bnymellon.com/login", external: true}]
    }, {
        header: "Bank Operations",
        link: "",
        items: [
            {name: "Check Void", link: ""},
            {name: "Check Copies", link: ""},
            {name: "Contribution Reversal/Adjustment", link: ""},
            {name: "Out of Balance", link: getOobLink(), external: true},
            {name: "Negative Bank Balances", link: ""},
            {name: "Card Fraud / Adjust Member Account", link: ""},
            {name: "Account Review before Closure & Liquidation", link: ""}]
    }, {
        header: "Cards",
        link: "",
        items: [
            {name: "Issue New Card", link: ""},
            {name: "Lost / Stolen Cards", link: ""},
            {name: "Cancel Card", link: ""},
            {name: "Card Request/Overnight", link: ""},
            {name: "Suspend/Unsuspend Card", link: ""},
            {name: "Delete Card", link: ""}
        ]
    }, {
        header: "Implementations",
        link: "",
        items: [
            {name: "Manage Paysite", link: ""}
        ]
    }, {
        header: "Quick Look Up",
        link: "/quicklookup",
        items: [
          {name: "View Contributions/Invoices by Employee", link: "/quicklookup/contributions"},
          {name: "View Employers by Employer ID or Name", link: "/quicklookup/employers"},
          {name: "View Balance Transfer Information", link: ""}
        ]
  }
];

const buildLines = (items) =>
    items.map(({name, link, external}) =>
        link === "" ?
            <Link to={link} key={name} className="a-link landing-card-link disabled-link">{name}</Link> :
            external ?
                <a href={link} key={name} className="a-link landing-card-link" target={"_blank"}>{name}</a> :
                <Link to={link} key={name} className="a-link landing-card-link">{name}</Link>);

const buildCard = ({header, items, link}) => {
    return (
        <Card styles="display: block" key={header}>
            <Link to={link} className={`a-link${link === "" ? " disabled-link" : ""}`} style={{fontWeight: 'bold', fontSize: '1.5em'}}  >{header}</Link>
            {buildLines(items)}
        </Card>)};

class Index extends React.Component {
    render() {
        return (
            <Card className="landing-container">
                {cards.map(i => buildCard(i))}
                </Card>
        )
    }
}

export default Index
