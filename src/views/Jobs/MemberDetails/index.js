import React from 'react'
import Card from "../../../components/Card";
import TextField from "../../../components/TextField";
import PageTitle from "../../../components/PageTitle";
import Loading from "../../../components/Loading";
import Button from "../../../components/Button";
import Enrollment from './Enrollment'
import ButtonBar from "../../../components/ButtonBar";
import Modal from 'components/Modal'
import Security from 'data/security'

import {jobActions} from '../index'

const jobStatusLookup = {
	1: "Active",
	2: "Terminated",
	3: "On Leave",
	4: "Retired",
	5: "Deceased"
};

class JobMemberDetails extends React.Component {

	renderModal() {

		const {
			employee: {firstName, lastName, ssnLast4},
			jobId
		} = this.props.memberDetails;

		return (
			<Modal
				header="Purge Job Details"
				onClose={() => this.props.toggleModal('purgeModal')}
				showClose={true}>
				<Card>
					<div className="job-purge-detail-row">
						<TextField label="Job ID" value={jobId}/>
						<TextField label="First Name" value={firstName}/>
						<TextField label="Last Name" value={lastName}/>
						<TextField label="SSN" value={ssnLast4}/>
					</div>
				</Card>
				<div className="margin-top">
					<Button className="button button-primary" events={{onClick: () => this.props.handleClick(jobActions.PURGE_JOB)()}}>Confirm Purge</Button>
					<Button className="button button-secondary margin-left" events={{onClick: () => this.props.toggleModal('purgeModal')}}>Cancel</Button>
				</div>
			</Modal>
		)
	}

	renderMemberDetails() {
		const {
			employee: {firstName, lastName, ssnLast4, id},
			enrollments,
			bankAddress1,
			bankAddress2,
			bankCity,
			bankState,
			bankZip,
			distributorName,
			effectiveDate,
			employeeId,
			employerName,
			jobId,
			jobStatus,
			partnerEmployeeId,
			sponsorName,
			enrollmentPartnerId,
			enrollmentSubmitterId,
			policyCount
		} = this.props.memberDetails;

		const sec = new Security();
		const showPurgeButton = sec.hasEveryRole(['CYCCEdit', 'accountmanager', 'toolrecon']) && policyCount === 0 ;

		return (
			<div>
				<Card>
					<PageTitle value={'Job Details'}/>
					<div className="job-detail-row">
						<TextField label="First Name" value={firstName}/>
						<TextField label="Last Name" value={lastName}/>
						<TextField label="SSN" value={ssnLast4}/>
						<TextField label="Partner Employee ID" value={partnerEmployeeId}/>
						<TextField label="Employee ID" value={employeeId}/>
						<TextField label="Person Id" value={id}/>
						<TextField label="Job ID" value={jobId}/>
						<TextField label="Employment Status" value={jobStatusLookup[jobStatus]}/>
						<TextField label="Employment Status Effective Date" value={effectiveDate}/>
						<TextField label="Eligible for Purge" value={policyCount > 0 ? "No" : "Yes"}/>
						{showPurgeButton && <ButtonBar actions={['Purge Profile']} onClick={() => this.props.toggleModal('purgeModal')}/>}
					</div>
				</Card>
				<Card>
					<PageTitle value={'Bank Address'}/>
					<div className="job-detail-row">
						<TextField label="Address Line 1" value={bankAddress1}/>
						<TextField label="Address Line 2" value={bankAddress2}/>
						<TextField label="City" value={bankCity}/>
						<TextField label="State" value={bankState}/>
						<TextField label="Zip Code" value={bankZip}/>
					</div>
				</Card>
				<Card>
					<PageTitle value={'Current Plan Year Enrollments'}/>
					{enrollments && enrollments.length > 0 ?
						enrollments.map((acc, idx) => <Enrollment key={idx} account={acc}/>) :
						"No Accounts Found"}
				</Card>
				<Card>
					<PageTitle value={'Employer Details'}/>
					<div className="job-detail-row">
						<TextField label="Employer Name" value={employerName}/>
						<TextField label="Sponsor Name" value={sponsorName}/>
						<TextField label="Partner Enrollment Id" value={enrollmentPartnerId}/>
						<TextField label="Enrollment Submitter Id"
											 value={enrollmentSubmitterId ? enrollmentSubmitterId : "NO_SUBMITTER"}/>
						<TextField label="Distributor/Sponsor" value={distributorName + "/" + sponsorName}/>
					</div>
				</Card>
			</div>
		)
	}

	render() {

		const {loading, memberDetails, purgeModal} = this.props;

		return (
			<Card>
				<PageTitle className={'account-title'} value={'Member Details'}/>
				{loading ?
					<Loading/> :
					memberDetails && this.renderMemberDetails()}
				{purgeModal && this.renderModal()}
			</Card>
		)
	}

}

export default JobMemberDetails;