import React from 'react'
import Formatters from "../../../../data/formatters";
import TextField from "../../../../components/TextField";
import FundingRuleData from '../../../../data/fundingRules'
import DrilldownTable from "../../../../components/DrilldownTable";
import Loading from "../../../../components/Loading";
import Card from "../../../../components/Card";
import Button from "../../../../components/Button";
import './JobEnrollments.scss'

const transactionCols = [
	{
		header: 'Status',
		property: 'status'
	},
	{
		header: 'Effective Date',
		property: 'effectiveDate'
	}, {
		header: 'Transaction Date',
		property: 'transactionDate',
	}, {
		header: 'Election Amount',
		getText: ({electionAmount}) => Formatters.currency(electionAmount)
	}, {
		header: 'Option Id',
		property: 'optionStatus',
	}, {
		header: 'Updated By',
		getText: ({firstName, lastName}) => !firstName ? lastName : `${lastName}, ${firstName}`
	}
];

class JobEnrollments extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			expanded: false,
			transactions: [],
			loading: true
		};
	}

	componentDidMount() {
		const data = new FundingRuleData();
		data.getTransactionsForFundingRule(this.props.account.policyId)
			.then(data => this.setState({transactions: data, loading: false}))
	}

	determineStatus = (today, expire, effective) => today > expire ? "Expired" : today < effective ? "Future" : "Active";
	clickHandler = () => this.setState({expanded: !this.state.expanded});

	render() {

		const {idx, account: {accountType, expireDate, effectiveDate, electionAmount, optionId}} = this.props;
		const status = this.determineStatus(new Date(), new Date(expireDate), new Date(effectiveDate));

		return(
			<div>
				<div className="job-account-detail-row" key={idx}>
					<div className="button-wrapper">
						<Button className="transactions-btn noSelect" events={{onClick: this.clickHandler}}>{this.state.expanded ? "-" : "+"}</Button>
					</div>
					<TextField label="Account Type" value={accountType}/>
					<TextField label="Enrollment Status" value={status}/>
					<TextField label="Enrollment Status Effective Date" value={effectiveDate}/>
					<TextField label="Election Amount" value={electionAmount} formatter={Formatters.currency}/>
					<TextField label="Option" value={optionId ? optionId : 'N/A'}/>
				</div>
				{this.state.loading ?
					<Loading/> :
					this.state.expanded ?
						<Card>
							<DrilldownTable
								cols={transactionCols}
								source={this.state.transactions}/>
						</Card>
						:
						null}
			</div>
		)
	}
}

export default JobEnrollments