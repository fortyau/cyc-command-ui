import React from 'react';
import Field from "../../../components/forms/Field";
import Card from "../../../components/Card";
import Loading from "../../../components/Loading";
import PageTitle from "../../../components/PageTitle";
import Button from "../../../components/Button";

import '../Jobs.scss'
import HSADetails from './HSADetails'
import {jobActions} from '../index'

const JobAccountDetails = props => {

    const {
        planYearFilter,
        accountTypeFilter,
        accountDetails,
        handleFilter,
        handleClick,
        accountsExpanded,
        accountsLoading
    } = props;

    const selectedAccount = accountDetails
        .filter(({planYearStart, accountType}) =>
            planYearFilter === new Date(planYearStart).getFullYear().toString() &&
            accountTypeFilter === accountType)
        .pop();

    const planYears = accountDetails.map(({planYearStart}) => {
        let py = new Date(planYearStart).getFullYear().toString();
        return {[py]: {name: py}};
    });

    const accountTypes = accountDetails.map(({accountType}) =>
        ({[accountType]: {name: accountType, disabled: accountType !== "Health Savings Account"}}));

    const planYearsDropdown = {
        name: 'Plan Year: ',
        type: 'dropdown',
        placeholder: '',
        events: {onChange: (e) => handleFilter(e, 'planYearFilter')},
        data: Object.assign({}, ...planYears)
    };

    const accountTypesDropdown = {
        name: 'Account Type: ',
        type: 'dropdown',
        placeholder: '',
        events: {onChange: (e) => handleFilter(e, 'accountTypeFilter')},
        data: Object.assign({}, ...accountTypes)
    };

    return (
        <Card className='margin-bottom'>
            <PageTitle className={'account-title'} value={'Account Details'}>
                <div className="account-btn-wrapper">
                    <Button className="account-btn noSelect"
                            events={{onClick: () => handleClick(jobActions.EXPAND_ACC)()}}>{accountsExpanded ? "-" : "+"}</Button>
                </div>
            </PageTitle>
            {accountsExpanded ?
                accountsLoading ?
                    <Loading/> :
                    accountDetails &&
                    <div>
                        <Field className={'account-dropdown'} {...planYearsDropdown}/>
                        <Field className={'account-dropdown'} {...accountTypesDropdown}/>
                        {selectedAccount && <HSADetails {...props} selectedAccount={selectedAccount}/>}
                    </div> :
                null}
        </Card>
    );

};

export default JobAccountDetails;