import React from 'react';
import Card from "../../../../components/Card";
import TextField from "../../../../components/TextField";
import Button from "../../../../components/Button";
import Modal from '../../../../components/Modal'
import SectionTitle from "../../../../components/SectionTitle";
import DrilldownTable from "../../../../components/DrilldownTable";
import Formatters from "../../../../data/formatters";
import Security from 'data/security'

import {jobActions} from '../../index'

const HSADetails = props => {

    const {
        toggleModal,
        handleClick,
        clearAccountModal,
        sendEnrollmentModal,
        planYearFilter,
        selectedAccount,
        memberDetails: {employee: {ssnLast4}, employerName, jobId}
    } = props;

    const {
        balance,
        planCode,
        processorId,
        status,
        effectiveDate,
        postings,
        fdrBalance,
        hsaAccountNumber,
        contributions,
        accountId,
        enrollmentsInfo
    } = selectedAccount;

    const renderClearAccountModal = () =>
        <Modal
            header="Clear Bank Account"
            onClose={() => toggleModal('clearAccountModal')}
            showClose={true}>
            <Card>
                <div className="clear-account-detail-row">
                    <TextField label="HSA Processor" value={processorId}/>
                    <TextField label="HSA Account Number" value={hsaAccountNumber}/>
                    <TextField label="Employer Name" value={employerName}/>
                    <TextField label="SSN" value={ssnLast4}/>
                    <TextField label="Job ID" value={jobId}/>
                </div>
            </Card>
            <div className="margin-top">
                <Button events={{onClick: () => handleClick(jobActions.CLEAR_ACC)(accountId)}} className="button button-primary">Submit</Button>
                <Button events={{onClick: () => toggleModal('clearAccountModal')}} className="button button-primary margin-left">Cancel</Button>
            </div>
        </Modal>;

    const enrollmentColsUMBHSAB = [{
        header: "File",
        property: "fileType"
    }, {
        header: "Send Status",
        property: "sendStatus"
    }, {
        header: "Send Date",
        property: "sendDate"
    },{
       header: "Account Status",
       property: "accountStatus"
    }, {
        header: "Send Enrollment",
        getText: ({oid, accountStatus}) =>
            accountStatus === "Pending" ?
                "Unable to send-Account Pending" :
            <Button events={{onClick: () => handleClick(jobActions.SEND_ENRL_UMBHSAB)(oid)}} className="button button-primary">Send</Button>
    }];

    // The process of sending an enrollment varies based on
    // the bank (processer id) of the account
    const renderSendEnrollmentModal = () => {
        switch (processorId) {
            case "Mellon":
            case "PFPC":
                return renderEnrollmentsModalBNY();
            case "HSA Bank":
            case "UMB Direct":
            case "CYC HSA Bank":
                return renderEnrollmentModalUMBHSAB();
            default:
                return null;
        }
    };

    const renderEnrollmentsModalBNY = () =>
        <Modal
            header="Send Enrollment"
            onClose={() => toggleModal('sendEnrollmentModal')}
            showClose={true}>
            <Card>
                <SectionTitle value="Update timpestamp for BNY Account to the below date:"/>
                <div className="send-enrollments-detail-row">
                    <TextField label="SSN" value={ssnLast4}/>
                    <TextField label="Transaction Date" value={enrollmentsInfo[0].transactionDate}/>
                    <TextField label="Effective Date" value={enrollmentsInfo[0].effectiveDate}/>
                </div>
                <Button events={{onClick: () => handleClick(jobActions.SEND_ENRL_BNY)(selectedAccount)}} className="button button-primary">Send</Button>
            </Card>
        </Modal>;


    const renderEnrollmentModalUMBHSAB = () =>
        <Modal
            header="Send Enrollment"
            onClose={() => toggleModal('sendEnrollmentModal')}
            showClose={true}>
            <Card>
                <SectionTitle value="Select the HSA Account File that would like to send for the following:"/>
                <div className="send-enrollments-detail-row">
                    <TextField label="Employer Name" value={employerName}/>
                    <TextField label="SSN" value={ssnLast4}/>
                    <TextField label="Job ID" value={jobId}/>
                </div>
                <DrilldownTable
                tableClass={'enrollment-table'}
                    cols={enrollmentColsUMBHSAB}
                    source={enrollmentsInfo}/>
            </Card>
        </Modal>;

    const yearMatcher = /[0-9]{4}/;
    const fees =
        postings
            .filter(({memo, settledTransactionDate}) => memo.includes('Fee') && settledTransactionDate.match(yearMatcher)[0] === planYearFilter)
            .map(({amount}) => amount)
            .reduce((t, c) => t + c, 0);

    const distributions =
        postings
            .filter(({memo, settledTransactionDate}) => memo.includes('Paid Claim') && settledTransactionDate.match(yearMatcher)[0] === planYearFilter)
            .map(({amount}) => amount)
            .reduce((t, c) => t + c, 0);

    const totalContributions =
        contributions
            .filter(([year, ,]) => year === planYearFilter)
            .map(([, , amount]) => amount)
            .reduce((t, c) => t + c, 0);

    // The following fields are required for HSAB Enrollments so we do not show the
    // button if there are not enrollments attached to the account

    const sec = new Security();
    const enrollmentsClearAccountAuth = sec.hasEveryRole(['CYCCEdit', 'accountmanager', 'toolrecon']);

    const showEnrollmentsButton =
        (processorId === ("HSA Bank" || "UMB Direct" || "CYC HSA Bank") ?
            enrollmentsInfo[0].fileType &&
            enrollmentsInfo[0].sendStatus &&
            enrollmentsInfo[0].sendDate &&
            enrollmentsInfo[0].oid
            : true) && enrollmentsClearAccountAuth;

    return (
        <div>
            <Card>
                <div className="job-detail-row">
                    <TextField label={"HSA Processor"} value={processorId}/>
                    <TextField label={"HSA Processor Plan Code"} value={planCode}/>
                    <TextField label={"CYC Account Status"} value={status}/>
                    <TextField label={"CYC Account Status Effective Date"} value={effectiveDate}/>
                    <TextField label={"HSA Account Number"} value={hsaAccountNumber}/>
                    <TextField label={"HSA Total Contributions"} value={totalContributions} formatter={Formatters.currency}/>
                    <TextField label={"HSA Balance"} value={balance} formatter={Formatters.currency}/>
                    <TextField label={"HSA Total Distributions"} value={Math.abs(distributions)} formatter={Formatters.currency}/>
                    <TextField label={"HSA Total Fees"} value={Math.abs(fees)} formatter={Formatters.currency}/>
                    {fdrBalance[1] ?
                        <TextField label="FDR Balance" formatter={Formatters.currency} value={fdrBalance[1]}/> :
                        <TextField label="FDR Balance" value='N/A'/>
                    }
                </div>
            </Card>
            {enrollmentsClearAccountAuth && <Button className="margin-top margin-right button button-primary" events={{onClick: () => toggleModal('clearAccountModal')}}>Clear Account Number</Button>}
            {showEnrollmentsButton && <Button className="margin-top margin-right button button-primary" events={{onClick: () => toggleModal('sendEnrollmentModal')}}>Send Enrollment</Button>}
            {clearAccountModal && renderClearAccountModal()}
            {sendEnrollmentModal && renderSendEnrollmentModal()}
        </div>
    )
};

export default HSADetails;