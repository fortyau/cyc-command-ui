import React from 'react'
import {Link} from 'react-router-dom'
import JobsData from "../../data/jobs";
import AccountDetails from './AccountDetails'
import MemberDetails from './MemberDetails'
import Card from "../../components/Card";
import Button from "../../components/Button";
import {snackbarActions} from 'stores/SnackbarStore'

import './Jobs.scss'
import PageTitle from "../../components/PageTitle";
import AccountsData from "../../data/accounts";
import EnrollmentsData from "../../data/enrollments"

export const jobActions = {
    PURGE_JOB: 'purgeJob',
    CLEAR_ACC: 'clearAccount',
    SEND_ENRL_UMBHSAB: 'sendEnrollmentUMBHSAB',
    SEND_ENRL_BNY: 'sendEnrollmentBNY',
    EXPAND_ACC: 'expandAccounts'
};

class JobDetails extends React.Component {
    constructor(props) {
        super(props);
        this.props.history.goBack.bind(this);
        this.state = {
            jobId: props.match.params.id,
            accountDetails: [],
            accountsLoading: true,
            memberLoading: true,
            memberDetails: {},
            planYearFilter: undefined,
            accountTypeFilter: undefined,
            accountsExpanded: false,
            purgeModal: false,
            purged: false,
            clearAccountModal: false,
            sendEnrollmentModal: false
        }
    }

    componentDidMount() {
        const data = new JobsData();
        data.getMemberDetails(this.state.jobId)
            .then(data => {
                this.setState({memberDetails: data, memberLoading: false})
            });
    }

    toggleModal = modal => this.setState({[modal]: !this.state[modal]});
    handleFilter = ({target: {value}}, fil) => this.setState({[fil]: value});
    handleClick = action => ({
        [jobActions.PURGE_JOB]: () => this.purgeJob(),
        [jobActions.CLEAR_ACC]: (payload) =>  this.clearAccount(payload),
        [jobActions.SEND_ENRL_UMBHSAB]: (payload) => this.sendEnrollmentUMBHSAB(payload),
        [jobActions.SEND_ENRL_BNY]: (payload) => this.sendEnrollmentBNY(payload),
        [jobActions.EXPAND_ACC]: () => this.expandAccounts()
    })[action];

    purgeJob = () => {
        const data = new JobsData();
        data.purgeJob(this.state.jobId)
            .then(response => {
                if (response) {
                    this.setState({purgeModal: false, purged: true});
                    snackbarActions.showSnackbar('Purged Job', 'info');
                }
            })
    };

    clearAccount = accId => {
        const data = new AccountsData();
        data.clearAccountNumber(accId)
            .then(response => {
                if (response) {
                    this.setState({clearAccountModal: false});
                    snackbarActions.showSnackbar('Cleared Account Number', 'info');
                }
            })
    };

    sendEnrollmentUMBHSAB = oid => {
        const data = new EnrollmentsData();
        data.sendEnrollmentUMBHSAB(oid)
            .then(response => {
                if (response) {
                    this.setState({sendEnrollmentModal: false});
                    snackbarActions.showSnackbar('Sent Enrollment', 'info')
                }
            });
    };

    sendEnrollmentBNY = ({accountId, enrollmentsInfo, ssn}) => {
        const body = {
            "accId": accountId,
			"statusId": 2,
            "transactionDate": enrollmentsInfo[0].transactionDate,
            "effectiveDate": enrollmentsInfo[0].effectiveDate,
            "ssn": ssn
        };
        const data = new EnrollmentsData();
        data.sendEnrollmentBNY(body)
            .then(response => {
                if (response) {
                    this.setState({sendEnrollmentModal: false});
                    snackbarActions.showSnackbar('Sent Enrollment', 'info')
                }
            })
    };

    expandAccounts = () => {
        const data = new JobsData();
        data.getAccountDetails(this.state.jobId)
            .then(data => {
                this.setState({accountDetails: data, accountsLoading: false})
            });
        this.setState({accountsExpanded: !this.state.accountsExpanded})
    };

    render() {

        const {
            accountDetails,
            planYearFilter,
            accountTypeFilter,
            memberLoading,
            memberDetails,
            purged,
            jobId,
            accountsExpanded,
            clearAccountModal,
            sendEnrollmentModal,
            purgeModal,
            accountsLoading
        } = this.state;

        return (
            <div>
                <Link to="/members"><Button className="button button-primary">New Member Search</Button></Link>
                <Button className="button button-secondary margin-left" events={{onClick: () => this.props.history.goBack()}}>Back
                    to Search Result</Button>
                {!purged ?
                    <div>
                        <MemberDetails
                            loading={memberLoading}
                            memberDetails={memberDetails}
                            toggleModal={this.toggleModal}
                            handleClick={this.handleClick}
                            purgeModal={purgeModal}/>
                        <AccountDetails
                            handleClick={this.handleClick}
                            toggleModal={this.toggleModal}
                            handleFilter={this.handleFilter}
                            accountsExpanded={accountsExpanded}
                            accountsLoading={accountsLoading}
                            clearAccountModal={clearAccountModal}
                            sendEnrollmentModal={sendEnrollmentModal}
                            accountDetails={accountDetails}
                            planYearFilter={planYearFilter}
                            accountTypeFilter={accountTypeFilter}
                            memberDetails={memberDetails}/>
                    </div> :
                    <Card>
                        <PageTitle value={`Job Id ${jobId} has been purged`}/>
                    </Card>}
            </div>
        )
    }
}

export default JobDetails