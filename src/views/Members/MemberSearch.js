import React from 'react';
import Field from "../../components/forms/Field";
import Button from "../../components/Button";

const memberSearchFields = {
	fields: {
		firstName: {
			name: 'firstName',
			label: 'First Name',
			placeholder: '',
			type: 'text',
			validator: null,
			required: false
		},
		lastName: {
			name: 'lastName',
			label: 'Last Name',
			placeholder: '',
			type: 'text',
			validator: null,
			required: false
		},
		ssn: {
			name: 'ssn',
			label: 'SSN',
			placeholder: '',
			type: 'text',
			validator: null,
			required: false
		},
		employeeId: {
			name: 'employeeId',
			label: 'Employee ID',
			placeholder: '',
			type: 'text',
			validator: null,
			required: false
		},
		partnerId: {
			name: 'partnerId',
			label: 'Partner Employee ID',
			placeholder: '',
			type: 'text',
			validator: null,
			required: false
		},
		jobId: {
			name: 'jobId',
			label: 'Job ID',
			placeholder: '',
			type: 'text',
			validator: null,
			required: false
		},
		employerName: {
			name: 'employerName',
			label: 'Employer Name',
			placeholder: '',
			type: 'text',
			validator: null,
			required: false
		}
	}
};

class MemberSearch extends React.Component {

	getMemberSearchFields() {
		const {fields} = memberSearchFields;

		return Object.entries(fields).map(field => {
			const [, values] = field;
			return {
				value: this.props.form[values.name] ? this.props.form[values.name] : "",
				...values,
				events: {
					onChange: event => this.props.handleChange(values.name, event.target.value)
				}
			}
		})
	}

	render() {
		return (
			<form style={{'display': 'flex', 'flexFlow': 'row wrap'}}
						onSubmit={this.props.handleSubmit}>
				{this.getMemberSearchFields().map((f, idx) => <Field className={'member-search-input'}
																	 key={`${f.name}-${idx}`} {...f}/>)}
				<div className={'form-formatting'}>
					<Button disabled={this.props.submitting} type='submit'
									className='button button-primary margin-top'>Search</Button>
				</div>
			</form>
		)
	}
}

export default MemberSearch;
