import React from 'react'
import Card from 'components/Card'
import PageTitle from 'components/PageTitle'
import MembersData from "../../data/members";
import './Members.scss'
import MemberSearch from "./MemberSearch";
import MemberSearchResults from "./MemberSearchResults";

class Members extends React.Component {
	constructor(props) {
		super(props);
		this.state = props.history.location.state ? props.history.location.state : {
			form: {},
			loading: false,
			limitResults: true,
			members: []
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleClick = this.handleClick.bind(this);
	}

	handleChange(name, value) {
		let form = {...this.state.form};
		form[name] = value;
		this.setState({form})
	}

	handleSubmit(event) {
		event.preventDefault();
		this.setState({loading: true, hasSearched: true});
		const data = new MembersData();
		const trimmedForm = Object.entries(this.state.form).reduce((acc, [k, v]) => {
            acc[k] = v.trim();
		    return acc
		}, {});
		data.getMembers(trimmedForm)
			.then(data => {
				this.setState({loading: false, members: data, limitResults: true});
				this.props.history.push("/members", this.state);
			})
	}

	handleClick() {
		this.setState({limitResults: !this.state.limitResults})
	}

	render() {
		const {submitting, form, members, limitResults, hasSearched, loading} = this.state;
		return (
			<Card>
				<PageTitle value="Find a Member" className={'form-formatting'}/>
				<MemberSearch
					handleSubmit={this.handleSubmit}
					submitting={submitting}
					form={form}
					handleChange={this.handleChange}/>
				<MemberSearchResults
					members={members}
					limitResults={limitResults}
					hasSearched={hasSearched}
					loading={loading}
					handleClick={this.handleClick}/>
			</Card>
		)
	}

}

export default Members