import React from 'react';
import DrilldownTable from 'components/DrilldownTable';
import Button from "../../components/Button";
import Formatters from "../../data/formatters";
import Loading from "../../components/Loading";

const cols = [
	{
		header: 'Name',
		getText: ({lastName, firstName}) => Formatters.lastCommaFirst(lastName, firstName)
	},
	{
		header: 'Job ID',
		property: 'jobId',
		isLink: true
	},
	{
		header: 'Employer Name',
		property: 'employerName'
	},
	{
		header: 'SSN (Last 4)',
		getText: ({ssn}) => ssn ? Formatters.ssnLast4(ssn) : 'Unknown'
	},
	{
		header: 'Employment Status',
		property: 'jobStatus'
	}
];

class MemberSearchResults extends React.Component {

	buildLink(member) {
		return `/jobs/` + member.jobId
	}

	render() {
		const {members, limitResults, hasSearched, loading} = this.props;
		return (
			<div>
				{loading ?
					<Loading/> :
					members && members.length > 0 ?
						<div className="margin-top form-formatting">
							<DrilldownTable
								cols={cols}
								source={limitResults ? members.slice(0, 10) : members}
								buildLink={this.buildLink}
							/>
							{members.length > 10 && <Button events={{onClick: this.props.handleClick}}>{limitResults ? "Show More" : "Show Less"}</Button>}
						</div> :
						hasSearched && <div className="margin-top">No members found</div>}
			</div>
		)
	}
}

export default MemberSearchResults