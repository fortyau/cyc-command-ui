var _Environments = {
  development: {
    COMMAND_DOMAIN: 'http://localhost:11090'
  },
  // staging: {
  //   ACCOUNT_DOMAIN: 'https://secure.dev.connectyourcare.net',
  //   EMAIL_DOMAIN: 'http://172.26.32.206:11090',
  //   FEE_DOMAIN: 'http://172.26.32.116:11090',
  //   INTEREST_PROFILE_DOMAIN: 'http://172.26.32.52:11090',
  //   PORTAL_DOMAIN: 'https://secure.dev.connectyourcare.net',
  //   SMS_DOMAIN: 'http://172.26.32.34:11090',
  //   CONFIG_DOMAIN: 'http://172.26.32.35:11090'
  // },
  // production: {
  //   ACCOUNT_DOMAIN: 'https://secure.dev.connectyourcare.net',
  //   EMAIL_DOMAIN: 'http://172.26.32.206:11090',
  //   FEE_DOMAIN: 'http://172.26.32.116:11090',
  //   INTEREST_PROFILE_DOMAIN: 'http://172.26.32.52:11090',
  //   PORTAL_DOMAIN: 'https://secure.dev.connectyourcare.net',
  //   SMS_DOMAIN: 'http://172.26.32.34:11090',
  //   CONFIG_DOMAIN: 'http://172.26.32.35:11090'
  // }
}

function getEnvironment () {
  // Insert logic here to get the current platform (e.g. staging, production, etc)
  // var platform = getPlatform()
  var platform = 'staging'

  // ...now return the correct environment
  return _Environments[platform]
}

var Environment = getEnvironment()
module.exports = Environment
