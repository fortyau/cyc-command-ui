# CYC Command UI

## Running and building locally

```
# node v8.9.4
npm install
npm start       # Start the dev server on your host machine
npm run build   # Create a production build in the /build folder
```


## Docker and Local Development

Install Docker for your platform and then...


### Local Development Environment

Standard `Dockerfile` provides a local dev environment, synced to the local file
system. Run the below commands and change any files in the `/src` folder to trigger
an automatic reload

```
docker build -t cyc-command .
docker run -it -v ${PWD}/src:/app/src -p 3000:3000 --rm cyc-command
```

**GOTCHAS**:

- Changes in `package.json` will require a rebuild.
- Actually, any change not in `./src` will require an image rebuild


### Production Docker

NOTE: Production has not been set up yet. The following was copied from the template and should not be run yet!

To run the node web server in production mode use the `Dockerfile-prod` file

This Dockerfile uses the node Docker image to build the application and then copies
that artifact into an nginx/alpine container

```
docker build -f Dockerfile-prod -t cyc-commannd-prod .

# To test a production build locally:
docker run -it -p 8080:80 --rm cyc-command-prod
```


Deploy to S3:
###STAGING
aws s3 mb s3://bucket-name

// list buckets
$ aws s3 ls

// build and deploy the app
$ npm run build && aws s3 sync build/ s3://bucket-name
$ npm run build && aws s3 sync build/ s3://cyc-react-web-staging
