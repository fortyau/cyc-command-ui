FROM node:8.9.4

ADD . /app
WORKDIR /app

RUN npm install

ENV PATH $PATH:/app/node_modules/.bin

CMD ["npm", "start"]
